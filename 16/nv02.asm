; Alexander Shabarshin (shaos@mail.ru)
; http://shaos.ru/nedopc/

  TITLE  "NedoVideo-16 v0.2"
  LIST R=DEC
  INCLUDE "p16f871.inc"

  CBLOCK 0x020
  counter	; counter for mWAIT3PLUS1
  counter_	; counter for Frame
  zerox		; start value for X counter (with SCR bit)
  tmpy		; temprary value of Y
  ENDC

  __CONFIG _CP_OFF & _DEBUG_OFF & _WRT_ENABLE_OFF & _CPD_OFF & _LVP_OFF & _PWRTE_ON & _WDT_OFF & _HS_OSC

; PORTA X0,X1,X2,X3,X4,SCR

nvSCR	EQU	5

; PORTB S0,S1,S2,Y0,Y1,Y2,Y3,Y4

; PORTC A0,A1,A2,SYN,DIR,_EN,_WR,_LD

nvSYN	EQU	3
nvDIR	EQU	4
nv_EN	EQU	5
nv_WR	EQU	6
nv_LD	EQU	7

; PORTD D0,D1,D2,D3,D4,D5,D6,D7

; PORTE _RST,ISTB,IMODE

nv_RST	EQU	0
nvISTB	EQU	1 ; strobe from LPT
nvIMODE EQU	2 ; 0-NTSC, 1-PAL

mBANK0 MACRO
  bcf STATUS,RP0
  bcf STATUS,RP1
  ENDM

mBANK1 MACRO
  bsf STATUS,RP0
  bcf STATUS,RP1
  ENDM

mBANK3 MACRO
  bcf STATUS,RP0
  bsf STATUS,RP1
  ENDM

mBANK4 MACRO
  bsf STATUS,RP0
  bsf STATUS,RP1
  ENDM

mNTSC3PAL5 MACRO
  LOCAL _ntsc
  btfss PORTE,nvIMODE
  goto _ntsc
  nop
  nop
  nop
_ntsc
  ENDM

mWAIT3PLUS1 MACRO Step
  LOCAL _loop
  movlw Step
  movwf counter
_loop
  decfsz counter,f
  goto _loop
  ENDM

mSYNC1 MACRO
  bsf PORTC,nvSYN
  ENDM

mSYNC0 MACRO
  bcf PORTC,nvSYN
  ENDM

mLINE9 MACRO Step,Subprog
  LOCAL _loop1,_loop2
  movlw Step
  movwf counter_
  goto _loop2
_loop1
  nop
  nop
  nop
  nop
_loop2
  call Subprog
  decfsz counter_
  goto _loop1
  nop
  ENDM

  ORG 00h ;reset vector
  goto Start

  ORG 04h ;Interrupt vector
  retfie

Start

  ;configure all I/O pins as digital (port A and E)
  mBANK1
  movlw 0x06
  movwf ADCON1

  ;set port direction
  mBANK1
  movlw b'11000000'
  movwf TRISA
  movlw b'00000000'
  movwf TRISB
  movlw b'00000000'
  movwf TRISC
  movlw b'00000000'
  movwf TRISD
  movlw b'00000110'
  movwf TRISE

  ;intialize values on ports
  mBANK0
  clrf PORTA
  clrf PORTB
  clrf PORTC
  clrf PORTD
  clrf PORTE
  bsf PORTC,nv_LD
  bsf PORTE,nv_RST

  ;setup interrupts
  mBANK1
  clrf INTCON ;disable all interrupts and clear all flags
  bsf OPTION_REG,NOT_RBPU ;disable pull-ups
;  bsf OPTION_REG,INTEDG ;interrupt on rising edge
;  bsf INTCON,INTE ;enable RB0 port change interrupt
;  bsf INTCON,GIE ;enable interrupts
 
  mBANK0
  CLRWDT
  movlw 0
  movwf zerox
  goto Frame

Line
  mSYNC0
  mWAIT3PLUS1 7 ; 22
  mSYNC1
  mWAIT3PLUS1 4 ; 13
  nop
  nop
  nop
  nop
  movf tmpy,w
  movwf PORTB
  bcf PORTE,nv_RST
  bsf PORTE,nv_RST
  movf zerox,w
  movwf PORTA ; 47
Symbol
  bcf PORTC,nv_LD
  bsf PORTC,nv_LD
  nop
  nop
  incf PORTA,f
  btfss PORTA,5
  goto Symbol
  incf tmpy,f
  clrf PORTB
  mNTSC3PAL5
  return

Line0
  mSYNC0		; 1
  mWAIT3PLUS1 7		; 22
  mSYNC1		; 1
  mWAIT3PLUS1 91	; 274
  nop                   ; 1
  nop			; 1
  nop                   ; 1
  clrf PORTA            ; 1
  clrf PORTB            ; 1
  clrf tmpy             ; 1
  mNTSC3PAL5            ; 3/5
  return                ; 2

Line1
  mSYNC1
  mWAIT3PLUS1 7
  mSYNC0
  mWAIT3PLUS1 91
  nop
  nop
  nop
  nop ; movlw 0xFF
  nop ; movwf PORTB
  nop
  mNTSC3PAL5
  return

Line2
  mSYNC1
  mWAIT3PLUS1 7
  mSYNC0
  mWAIT3PLUS1 45
  mSYNC1
  mWAIT3PLUS1 46
  nop
  nop
  nop ; movlw 0xFF
  nop ; movwf PORTB
  nop
  mNTSC3PAL5
  return

; Specially for PAL

Line0p ; 320-9=311
  mSYNC0		; 1
  mWAIT3PLUS1 3		; 10
  mSYNC1		; 1
  mWAIT3PLUS1 49	; 148
  mSYNC0		; 1
  mWAIT3PLUS1 3		; 10
  mSYNC1		; 1
  mWAIT3PLUS1 45	; 136
  nop			; 1
  return		; 2

Line1p ; 320-9=311
  mSYNC1		; 1
  mWAIT3PLUS1 45	; 136
  mSYNC0		; 1
  mWAIT3PLUS1 7		; 22
  mSYNC0		; 1
  mWAIT3PLUS1 45	; 136
  mSYNC1		; 1
  mWAIT3PLUS1 3		; 10
  nop			; 1
  return		; 2

Line2p ; 320-9=311
  mSYNC1		; 1
  mWAIT3PLUS1 45	; 136
  mSYNC0		; 1
  mWAIT3PLUS1 7		; 22
  mSYNC0		; 1
  mWAIT3PLUS1 3		; 10
  mSYNC1		; 1
  mWAIT3PLUS1 45	; 136
  nop			; 1
  return		; 2

Frame
  btfss PORTE,nvIMODE
  goto FrameN

FrameP ; PAL
;Frame (in lines) 312:
;25-sync 47-preframe 192-frame 48-postframe
;Line (in ticks) 320:
;24-low 30-high 256-signal 10-high
  nop
;Sync
  mLINE9 3,Line0p
  mLINE9 2,Line1p
  mLINE9 1,Line2p
  mLINE9 2,Line0p
  mLINE9 17,Line0
  mLINE9 47,Line0
  mLINE9 192,Line
  mLINE9 48,Line0
  ; -5 
  goto Frame

FrameN ; NTSC
;Frame (in lines) 262:
;20-sync 24-preframe 192-frame 26-postframe
;Line (in ticks) 318:
;24-low 30-high 256-signal 8-high
  mLINE9 3,Line0
  mLINE9 3,Line1
  mLINE9 14,Line0
  mLINE9 24,Line0
  mLINE9 192,Line
  mLINE9 26,Line0
  ; -5
  goto Frame

  END
