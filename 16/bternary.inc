; bternary.inc - some constants for binary coded ternary (20 Nov 2011)
; created by Alexander A. Shabarshin <ashabarshin@gmail.com> for "3niti alpha simu1"

;Format of the triad:
;|7|6|5|4|3|2|1|0|
;|+|-|+|-|+|-|+|-|
;|par|low|mid|high
;par (parity) is not yet used, so it's always 0 0 for now...

tOOO	EQU	0x00 ; 0
tNOO	EQU	0x01 ; -9
tPOO	EQU	0x02 ; +9
tONO	EQU	0x04 ; -3
tNNO	EQU	0x05 ; -12
tPNO	EQU	0x06 ; +6
tOPO	EQU	0x08 ; +3
tNPO	EQU	0x09 ; -6
tPPO	EQU	0x0A ; +12
tOON	EQU	0x10 ; -1
tNON	EQU	0x11 ; -10
tPON	EQU	0x12 ; +8
tONN	EQU	0x14 ; -4
tNNN	EQU	0x15 ; -13
tPNN	EQU	0x16 ; +5
tOPN	EQU	0x18 ; +2
tNPN	EQU	0x19 ; -7
tPPN	EQU	0x1A ; +11
tOOP	EQU	0x20 ; +1
tNOP	EQU	0x21 ; -8
tPOP	EQU	0x22 ; +10
tONP	EQU	0x24 ; -2
tNNP	EQU	0x25 ; -11
tPNP	EQU	0x26 ; +7
tOPP	EQU	0x28 ; +4
tNPP	EQU	0x29 ; -5
tPPP	EQU	0x2A ; +13
