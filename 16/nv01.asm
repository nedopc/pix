  TITLE  "NedoVideo-16 v0.1"
  LIST R=DEC
  INCLUDE "p16f871.inc"

  CBLOCK 0x020
  counter ; counter for mWAIT3PLUS1
  zerox   ; start value for X counter (with SCR bit)
  tmpy    ; temprary value of Y
  ENDC

  __CONFIG _CP_OFF & _DEBUG_OFF & _WRT_ENABLE_OFF & _CPD_OFF & _LVP_OFF & _PWRTE_ON & _WDT_OFF & _HS_OSC

; PORTA X0,X1,X2,X3,X4,SCR

nvSCR	EQU	5

; PORTB S0,S1,S2,Y0,Y1,Y2,Y3,Y4

; PORTC A0,A1,A2,SYN,DIR,_EN,_WR,_LD

nvSYN	EQU	3
nvDIR	EQU	4
nv_EN	EQU	5
nv_WR	EQU	6
nv_LD	EQU	7

; PORTD D0,D1,D2,D3,D4,D5,D6,D7

; PORTE _RST,ISTB,IMODE

nv_RST	EQU	0
nvISTB	EQU	1 ; strobe from LPT
nvIMODE EQU	2 ; 0-NTSC, 1-PAL

mBANK0 MACRO
  bcf STATUS,RP0
  bcf STATUS,RP1
  ENDM

mBANK1 MACRO
  bsf STATUS,RP0
  bcf STATUS,RP1
  ENDM

mBANK3 MACRO
  bcf STATUS,RP0
  bsf STATUS,RP1
  ENDM

mBANK4 MACRO
  bsf STATUS,RP0
  bsf STATUS,RP1
  ENDM

mNTSC3PAL5 MACRO
  LOCAL _ntsc
  btfss PORTE,nvIMODE
  goto _ntsc
  nop
  nop
  nop
_ntsc
  ENDM

mWAIT3PLUS1 MACRO Step
  LOCAL _loop
  movlw Step
  movwf counter
_loop
  decfsz counter,f
  goto _loop
  ENDM

mSYNC1 MACRO
  bsf PORTC,nvSYN
  ENDM

mSYNC0 MACRO
  bcf PORTC,nvSYN
  ENDM

  ORG 00h ;reset vector
  goto Start

  ORG 04h ;Interrupt vector
  retfie

Start

  ;configure all I/O pins as digital (port A and E)
  mBANK1
  movlw 0x06
  movwf ADCON1

  ;set port direction
  mBANK1
  movlw b'11000000'
  movwf TRISA
  movlw b'00000000'
  movwf TRISB
  movlw b'00000000'
  movwf TRISC
  movlw b'00000000'
  movwf TRISD
  movlw b'00000110'
  movwf TRISE

  ;intialize values on ports
  mBANK0
  clrf PORTA
  clrf PORTB
  clrf PORTC
  clrf PORTD
  clrf PORTE
  bsf PORTC,nv_LD
  bsf PORTE,nv_RST

  ;setup interrupts
  mBANK1
  clrf INTCON ;disable all interrupts and clear all flags
  bsf OPTION_REG,NOT_RBPU ;disable pull-ups
;  bsf OPTION_REG,INTEDG ;interrupt on rising edge
;  bsf INTCON,INTE ;enable RB0 port change interrupt
;  bsf INTCON,GIE ;enable interrupts
 
  mBANK0
  CLRWDT
  movlw 0
  movwf zerox
  goto FrameN

Line
  mSYNC0
  mWAIT3PLUS1 7 ; 22
  mSYNC1
  mWAIT3PLUS1 7 ; 22
  nop
  nop
  movf tmpy,w
  movwf PORTB
  bcf PORTE,nv_RST
  bsf PORTE,nv_RST
  movf zerox,w
  movwf PORTA ; 54
Symbol
  bcf PORTC,nv_LD
  bsf PORTC,nv_LD
 IF 1
  nop
  nop
  incf PORTA,f
  btfss PORTA,5
 ELSE
  incf PORTA,w
  andlw 0x1F
  movwf PORTA
  btfss STATUS,Z
 ENDIF
  goto Symbol
  incf tmpy,f
  clrf PORTB
  mNTSC3PAL5
  return

Line0
  mSYNC0
  mWAIT3PLUS1 7
  mSYNC1
  mWAIT3PLUS1 94
  nop
  clrf PORTA
  clrf PORTB
  clrf tmpy
  mNTSC3PAL5
  return

Line1
  mSYNC1
  mWAIT3PLUS1 7
  mSYNC0
  mWAIT3PLUS1 94
  nop
  nop ; movlw 0xFF
  nop ; movwf PORTB
  nop
  mNTSC3PAL5
  return

FrameN
  call Line0
  call Line0
  call Line0
  call Line1
  call Line1
  call Line1
  call Line0
  call Line0
  call Line0
  call Line0
  call Line0
  call Line0
  call Line0
  call Line0
  call Line0
  call Line0
  call Line0
  call Line0
  call Line0
  call Line0

  call Line0
  call Line0
  call Line0
  call Line0
  call Line0
  call Line0
  call Line0
  call Line0
  call Line0
  call Line0
  call Line0
  call Line0
  call Line0
  call Line0
  call Line0
  call Line0
  call Line0
  call Line0
  call Line0
  call Line0
  call Line0
  call Line0
  call Line0
  call Line0

  call Line
  call Line
  call Line
  call Line
  call Line
  call Line
  call Line
  call Line
  call Line
  call Line
  call Line
  call Line
  call Line
  call Line
  call Line
  call Line
  call Line
  call Line
  call Line
  call Line
  call Line
  call Line
  call Line
  call Line
  call Line
  call Line
  call Line
  call Line
  call Line
  call Line
  call Line
  call Line
  call Line
  call Line
  call Line
  call Line
  call Line
  call Line
  call Line
  call Line
  call Line
  call Line
  call Line
  call Line
  call Line
  call Line
  call Line
  call Line
  call Line
  call Line
  call Line
  call Line
  call Line
  call Line
  call Line
  call Line
  call Line
  call Line
  call Line
  call Line
  call Line
  call Line
  call Line
  call Line
  call Line
  call Line
  call Line
  call Line
  call Line
  call Line
  call Line
  call Line
  call Line
  call Line
  call Line
  call Line
  call Line
  call Line
  call Line
  call Line
  call Line
  call Line
  call Line
  call Line
  call Line
  call Line
  call Line
  call Line
  call Line
  call Line
  call Line
  call Line
  call Line
  call Line
  call Line
  call Line
  call Line
  call Line
  call Line
  call Line
  call Line
  call Line
  call Line
  call Line
  call Line
  call Line
  call Line
  call Line
  call Line
  call Line
  call Line
  call Line
  call Line
  call Line
  call Line
  call Line
  call Line
  call Line
  call Line
  call Line
  call Line
  call Line
  call Line
  call Line
  call Line
  call Line
  call Line
  call Line
  call Line
  call Line
  call Line
  call Line
  call Line
  call Line
  call Line
  call Line
  call Line
  call Line
  call Line
  call Line
  call Line
  call Line
  call Line
  call Line
  call Line
  call Line
  call Line
  call Line
  call Line
  call Line
  call Line
  call Line
  call Line
  call Line
  call Line
  call Line
  call Line
  call Line
  call Line
  call Line
  call Line
  call Line
  call Line
  call Line
  call Line
  call Line
  call Line
  call Line
  call Line
  call Line
  call Line
  call Line
  call Line
  call Line
  call Line
  call Line
  call Line
  call Line
  call Line
  call Line
  call Line
  call Line
  call Line
  call Line
  call Line
  call Line
  call Line
  call Line
  call Line
  call Line
  call Line
  call Line

  call Line0
  call Line0
  call Line0
  call Line0
  call Line0
  call Line0
  call Line0
  call Line0
  call Line0
  call Line0
  call Line0
  call Line0
  call Line0
  call Line0
  call Line0
  call Line0
  call Line0
  call Line0
  call Line0
  call Line0
  call Line0
  call Line0
  call Line0
  call Line0
  call Line0
  call Line0

  goto FrameN

  END
