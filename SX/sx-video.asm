; sx-video.asm - Alexander Shabarshin (05 Nov 2006 - 03 Jan 2007)
; http://www.nedopc.org <alexander@shabarshin.com>
; Software version 1.0.0 for NedoText board v1.0 with NedoCPU-28+
; Code for 60MHz oscillator (HS3)
; This version is NTSC only!
; I/O pins:
; RTCC - not connected
; RA0 - output SYNC (video synchro)
; RA1 - output OFLAG (device busy flag)
; RA2 - output _LD (load byte to shift register)
; RA3 - output _PE (load zero to counter-divider by 6)
; RB0 - output ADR0 (bit 0 of memory address)
; RB1 - output ADR1 (bit 1 of memory address)
; RB2 - output ADR2 (bit 2 of memory address)
; RB3 - output ADR3 (bit 3 of memory address)
; RB4 - output DADR (0 - memory, 1 - registers)
; RB5 - output _RD (read from memory or input register/buffer)
; RB6 - output _WR (write to memory or address register)
; RB7 - input IFLAG (data ready flag)
; RC0...RC7 - input/output DAT0...DAT7

	processor sx28
	radix dec
	include "sxdefs.inc"

device	equ	pins28+oschs3+stackoptx+turbo

	id	'<', 'N', 'e', 'd', 'o', 'P', 'C', '>'

cnt1	equ	0x08 ; 1st counter 
cnt2	equ	0x09 ; 2nd counter
line	equ     0x0A ; low byte of line number in frame
lineh	equ	0x0B ; high byte of line number in frame
chara	equ	0x0C ; character line address
chary	equ	0x0D ; current row in character 
mempg	equ	0x0E ; memory page in current line
temp	equ	0x0F ; temporary variable

; bank 0

flags	equ	0x10 ; flags (0-_NTSC/PAL,1-IFLAG)
xcur	equ	0x11 ; x-coordinate of cursor (in characters)
ycur	equ	0x12 ; y-coordinate of cursor (in characters)
lcur	equ	0x13 ; y-coordinate of cursor (line of raster)
escom	equ	0x14 ; command or 0 for regular character
char	equ	0x15 ; character

vmode_f	equ	0
iflag_f	equ	1
oflag_f	equ	2

sync_a	equ	0
oflag_a	equ	1
_ld_a	equ	2
_pe_a	equ	3

adr0_b	equ	0
adr1_b	equ	1
adr2_b	equ	2
adr3_b	equ	3
dadr_b	equ	4
_rd_b	equ	5
_wr_b	equ	6
iflag_b	equ	7

cx_max	equ	80
cy_max	equ	25

y_s0	equ	201
y_s1	equ	225
y_s0e	equ	228

	include "shaosx.inc"

portc_output macro ; 2
	movlw	0x00
	tris	portc
	endm

portc_input macro ; 2
	movlw	0xFF
	tris	portc
	endm

sync0	macro ; 290
	bcf	porta,sync_a
	delay1	72,cnt1
	bsf	porta,sync_a
	endm

sync1	macro ; 290
	bsf	porta,sync_a
	delay1	72,cnt1
	bcf	porta,sync_a
	endm

mem_page macro ; 8 (W - 4..11 bits of address)
	movwf	portc
	portc_output ; 2
	bsf	portb,dadr_b
	nop
	bcf	portb,_wr_b
	nop
	bsf	portb,_wr_b
	endm
	
mem_ladr macro ; 2 (W - low 4 bits of address)	
	iorlw	0xF0
	movwf	portb
	endm

mem_write macro ; 8 (write W to memory)
	movwf	portc
	portc_output ; 2
	bcf	portb,dadr_b
	nop
	bcf	portb,_wr_b
	nop
	bsf	portb,_wr_b
	endm
	
mem_read macro ; 11 (read from memory to W)
	nop
	bcf	portb,dadr_b
	nop
	bcf	portb,_rd_b
	portc_input ; 2
	nop3
	movf	portc,w
	bsf	portb,_rd_b
	endm

com_read macro ; 11 (read from buffer to W)
	nop
	bsf	portb,dadr_b
	nop
	bcf	portb,_rd_b
	portc_input ; 2
	nop3
	movf	portc,w
	bsf	portb,_rd_b
	endm

set_oflag macro ; 2 (bank0)
	bsf	porta,oflag_a
	bsf	flags,oflag_f
	endm

clr_oflag macro ; 2 (bank0)
	bcf	porta,oflag_a
	bcf	flags,oflag_f
	endm

;<><><> C O D E <><><>;

int:	org	0x00
	bank0
	btfsc	lineh,0
	goto	l_s0
	movlw	y_s0
	subwf	line,w	
	btfsc	status,cf
	goto	l_s0
	delay1	3,cnt1	
	sync0	
	delay1	100,cnt1 
	movf	line,w
	andlw	0x07
	movwf	chary
	btfsc	status,zf
	goto	lacur
	call	chline
	goto	lend
lacur:	call	curline
	goto	lend	
l_s0:	movlw	y_s1
	subwf	line,w
	btfsc	status,cf
	goto	l_s1
	delay1	2,cnt1
	sync0
	goto	lend
l_s1:	movlw	y_s0e
	subwf	line,w
	btfsc	status,cf
	goto	l_s0e
	delay1	1,cnt1
	sync1
	goto	lend
l_s0e:	sync0
lend:	call	command
	incfsz	line,f
	goto	lend_
	incf	lineh,f
lend_:	btfss	lineh,0
	goto	lend__
	movlw	6
	subwf	line,w
	btfss	status,zf
	goto	lend__
	clrf	lineh
	clrf	line
lend__:	movlw	18
	retiw

chline: decf	chary,f
	movlr	0x70,fsr
	bcf	porta,_pe_a
	nop
	bsf	porta,_pe_a
chloop:	bsf	fsr,4 ; 1
	movlw	0x10 ; 2
	addwf	indf,w ; 3
	movwf	chara ; 4
	clrf	temp ; 5
	rlf	chara,f ; 6
	rlf	temp,f ; 7
	rlf	chara,f ; 8
	rlf	temp,f ; 9
	rlf	chara,f ; 10
	rlf	temp,w ; 11
	movwm ; 12
	movf	chary,w ; 13
	addwf	chara,w ; 14
	iread ; +4=18
	mode	0xF ; 19
	movwf	temp ; 20
	portc_output ; +2=22
	movf	temp,w ; 23
	nop ; 24
	movwf	portc ; 25
	nop ; 26
	bcf	porta,_ld_a ; 28
	nop ; 27
	nop ; 29
	bsf	porta,_ld_a ; 30
	nop ; 31
	incfsz	fsr,f ; 32
	goto	chloop ; +3=35 (it looks like 36)
	return

curline: 
; mempg=line/8*80/16=5/8*line=((line>>3)<<2)+(line>>3)=(line>>1)+(line>>3)
	bcf	status,cf
	rrf	line,w
	movwf	mempg
	movwf	temp
	bcf	status,cf
	rrf	temp,f
	bcf	status,cf
	rrf	temp,w
	addwf	mempg,f
	; 5*(2+329)=1655 (47 characters)
	bank3
	call	pageread
	incf	mempg,f
	bank4 
	call	pageread
	incf	mempg,f
	bank5
	call	pageread
	incf	mempg,f
	bank6
	call	pageread
	incf	mempg,f
	bank7
	call	pageread
	return
		
pageread: ; 3(call)+417=420
	movf	mempg,w ; 1
	mem_page ; 8
	call	mem_read16 ; 405
	return ; 3

mem_write16: ; 3(call)+5+16*17-2+3=281
	bsf	fsr,4
	bcf	fsr,3
	bcf	fsr,2
	bcf	fsr,1
	bcf	fsr,0
mem_w16:	
	movf	fsr,w
	iorlw	0xF0
	movwf	portb
	movf	indf,w
	mem_write ; 8
	incf	fsr,f
	btfsc	fsr,4
	goto	mem_w16
	return

mem_read16: ; 3(call)+5+16*20-2+3=329
	bsf	fsr,4
	bcf	fsr,3
	bcf	fsr,2
	bcf	fsr,1
	bcf	fsr,0
mem_r16:
	movf	fsr,w
	iorlw	0xF0
	movwf	portb
	mem_read ; 11
	movwf	indf
	incf	fsr,f
	btfsc	fsr,4
	goto	mem_r16
	return

ntsc_pal: ; 3(call)+13=16 (bank0)
	bcf	portb,dadr_b
	nop3
	btfss	portb,iflag_b	
	goto ntsc
	bsf	flags,vmode_f
	goto	ntsc_pal_end
ntsc:	nop
	bcf	flags,vmode_f
ntsc_pal_end:
	return	

get_iflag: ; 3(call)+13=16 (bank0)
	bsf	portb,dadr_b
	nop3
	btfss	portb,iflag_b	
	goto noifl
	bsf	flags,iflag_f
	goto	get_iflag_end
noifl:	nop
	bcf	flags,iflag_f
get_iflag_end:
	return
	
command: ; 3(call)+103(max)=106 (118 in case of error)
	bank0 ; 1
	btfss	flags,oflag_f ; 2
	goto	command0 ; +3=5
	; oflag=1
	call	get_iflag ; 16
	btfsc	flags,iflag_f
	return ; iflag=1
	; iflag=0
	clr_oflag ; 2
	return
command0: 
	; oflag=0
	call	get_iflag ; +16=21
	btfss	flags,iflag_f ; 22
	return ; iflag=0 (25)
	; iflag=1
	com_read ; +11=36
	movwf	char ; 37
	set_oflag ; +2=39
	movf	escom,f ; 40
	btfsc	status,zf ; 41
	goto	com_char ; 42/44
	; argument for command
	movlw	0x01 ; 43
	subwf	escom,w ; 44
	btfss	status,zf ; 45
	goto	command1 ; 46/48
	; escom=0x01 (x)
	movf	char,w ; 47
	movwf	xcur ; 48
	clrf	escom ; 49
	return ; 52
command1:	
	movlw	0x02 ; 49
	subwf	escom,w ; 50
	btfss	status,zf ; 51
	goto	com_char_noesc ; 52/55
	; escom=0x02 (y)
	movf	char,w ; 53
	movwf	ycur ; 54
	bcf	status,cf ; 55
	rlf	char,f ; 56
	rlf	char,f ; 57
	rlf	char,f ; 58
	movf	char,w ; 59
	movwf	lcur ; 60
	clrf	escom ; 61
	return ; 64
com_char_noesc: ; try as character
	clrf	escom ; 56
com_char: ; character
	movlw	0x20 ; 45 (or +12 in case of error command)
	subwf	char,w ; 46
	btfsc	status,cf ; 47
	goto	com_charr ; 48/50
	; char < 0x20
	movf	char,w ; 49
	movwf	escom ; 50
	return ; 53
com_charr:
	swapf	xcur,w ; 51
	andlw	0x0F ; 52
	movwf	temp ; 53
	bcf	status,cf ; 54
	rlf	ycur,w ; 55
	movwf	mempg ; 56
	rlf	mempg,w ; 57
	addwf	ycur,w ; 58
	addwf	temp,w ; 59
	movwf	mempg ; 60
	movlw	0x0F ; 61
	andwf	xcur,w ; 62
	mem_ladr ; +2=64
	movf	mempg,w ; 65
	mem_page ; +8=73
	movf	char,w ; 74
	mem_write ; +8=82
	incf	xcur,f ; 83
	movlw	cx_max ; 84
	subwf	xcur,w ; 85
	btfss	status,zf ; 86
	return ; 87/89
	clrf	xcur ; 88
	incf	ycur,f ; 89
	movlw	cy_max ; 90
	subwf	ycur,w ; 91
	btfss	status,zf ; 92
	goto com_end ; 93/95
	clrf	ycur ; 94
com_end:
	movf	ycur,w	; 95
	movwf	lcur ; 96
	bcf	status,cf ; 97
	rlf	lcur,f ; 98
	rlf	lcur,f ; 99
	rlf	lcur,f ; 100
	return ; 103

if 0

mem_page_sub:
	mem_page
	return

mem_f16: ; W - first value
	movwf	temp
	movwf	0x10
	incf	temp,f
	movf	temp,w
	movwf	0x11
	incf	temp,f
	movf	temp,w
	movwf	0x12
	incf	temp,f
	movf	temp,w
	movwf	0x13
	incf	temp,f
	movf	temp,w
	movwf	0x14
	incf	temp,f
	movf	temp,w
	movwf	0x15
	incf	temp,f
	movf	temp,w
	movwf	0x16
	incf	temp,f
	movf	temp,w
	movwf	0x17
	incf	temp,f
	movf	temp,w
	movwf	0x18
	incf	temp,f
	movf	temp,w
	movwf	0x19
	incf	temp,f
	movf	temp,w
	movwf	0x1A
	incf	temp,f
	movf	temp,w
	movwf	0x1B
	incf	temp,f
	movf	temp,w
	movwf	0x1C
	incf	temp,f
	movf	temp,w
	movwf	0x1D
	incf	temp,f
	movf	temp,w
	movwf	0x1E
	incf	temp,f
	movf	temp,w
	movwf	0x1F
	call mem_write16
	return

endif	

reset:
	mode	0x0E
	movlw	0x00
	tris	portc
	mode 	0x0F
	movlw	0x00
	tris	porta
	movlw	0x80
	tris	portb
	portc_output
	clrf	portc
	movlr	0x0F,porta
	movlr	0xF0,portb
	bank0
	clrf	lineh
	clrf	line
	clrf	flags
	clrf	xcur
	clrf	ycur
	clrf	escom
	movlw	0x08
	movwf	lcur
	call	ntsc_pal
	clr_oflag

if 0
	
	bank3
	movlw	0x00
	call mem_page_sub
	movlw	0x20
	call	mem_f16

	bank4
	movlw	0x01
	call mem_page_sub
	movlw	0x30
	call	mem_f16

	bank5
	movlw	0x02
	call mem_page_sub
	movlw	0x40
	call	mem_f16

	bank6
	movlw	0x03
	call mem_page_sub
	movlw	0x50
	call	mem_f16

	bank7
	movlw	0x04
	call mem_page_sub
	movlw	0x60
	call	mem_f16

	bank3
	movlw	0x05
	call mem_page_sub
	movlw	0x70
	call	mem_f16

	bank4
	movlw	0x06
	call mem_page_sub
	movlw	0x80
	call	mem_f16

	bank5
	movlw	0x07
	call mem_page_sub
	movlw	0x90
	call	mem_f16

	bank6
	movlw	0x08
	call mem_page_sub
	movlw	0xA0
	call	mem_f16

	bank7
	movlw	0x09
	call mem_page_sub
	movlw	0xB0
	call	mem_f16

	bank3
	movlw	0x0A
	call mem_page_sub
	movlw	0xC0
	call	mem_f16

	bank4
	movlw	0x0B
	call mem_page_sub
	movlw	0xD0
	call	mem_f16

	bank5
	movlw	0x0C
	call mem_page_sub
	movlw	0xE0
	call	mem_f16

endif

	options	0x13 ; 00010011 - RTCC prescaler 1:16
	
loop:	nop
	nop
	nop
	goto	loop

	org	0x180
include "sx-font.inc"
	org	0x7FF
	goto	reset

	END
