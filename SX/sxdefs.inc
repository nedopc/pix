	NOLIST
;
;SXDEFS.INC	by Loren Blaney and Richard Ottosen	14-FEB-2000
;
;Scenix SX Definitions for Microchip MPASM.
;
;REVISIONS:
;FEB-23-98, Released.
;MAR-21-98, Added ID label. Corrected XT & HS defs by swapping them.
; Removed ASCII defs. New STATUS defs.
;MAR-27-98, Added PAGEA, BANKA, FCALL, FGOTO, SKIP.
;APR-13-98, Added CSA, CSBE (etc.) macros. Enclose all arguments in parentheses.
;  Indent macros.
;APR-23-98, Changed some comments.
;OCT-4-98, Removed "RADIX DEC", added processor type based on SX FUSEX bits,
;  added Trim bits to FUSEX and other cleanup.   R.O.
;OCT-14-98 BOSC defaults to a "1".
;NOV-4-98, Revised Pins, Trim bits and BOSC in DEVICE equates, removed some
;   inversions.  R.O.
;SEP-11-99, Added warnings and messages to BANK and PAGE macros.  R.O.
;9-JAN-2000, Changed ID bytes to leave unused bits as ones.  R.O.
;12-JAN-2000, Made variables in macros local.  R.O.
;14-FEB-2000, Cleanup.  R.O.
;26-JUL-2001, Abbreviated version from servid project 
;   http://sourceforge.net/projects/servid
;17-NOV-2003, Updated FUSE/FUSEX bits per Ubicom SX20/AC/SX28AC Mar 2002 
;   spec.  Old definitions are available if _oldfuse == 1. J.G.
;17-NOV-2003, Convert to lower case for gpasm. J.G.
;17-NOV-2003, Add wfile def from Jim Paris's version. J.G.

_oldfuse equ	0		; set to 1 for old FUSE/FUSEX defs

f	equ	1
w	equ	0

;Define special function registers:
indf	equ	00h		;used for indirects thru fsr
wfile	equ	01h		;w file register (depending on option reg)
rtcc	equ	01h		;real time clock/counter
pcl	equ	02h		;low 8 bits of PC
status	equ	03h		;status bits
fsr	equ	04h		;file select register
porta	equ	05h		;I/O ports
portb	equ	06h		;supports multi-input wake-up (MIWU)
portc	equ	07h

;Define STATUS register bits:
cf	equ	0		;carry
dcf	equ	1		;digit carry
zf	equ	2		;zero
pdf	equ	3		;sleep power down (true low)
tof	equ	4		;watchdog time out (true low)
pa0	equ	5		;page select (LSB)
pa1	equ	6		;page select
pa2	equ	7		;page select (MSB)

;Define port control registers:
trisx	equ	0Fh		;tristate (1=input, 0=output)
plp	equ	0Eh		;pullup (1=none, 0=20k)
lvl	equ	0Dh		;level (1=TTL, 0=CMOS)
st	equ	0Ch		;Schmitt trigger (1=disabled, 0=enabled)
wken	equ	0Bh		;wake up (1=disabled, 0=enabled)
wked	equ	0Ah		;wake up edge (1=falling, 0=rising)
wkpnd	equ	09h		;wake up pending (1=pending, 0=none)
cmp	equ	08h		;comparator bit: 0=result, 6=output, 7=enabled 

	if	(_oldfuse == 0)

;Define device symbols for configuration words (FUSE & FUSEX)
;per Ubicom SX20AC/SX28AC spec dated March 2002

oscrc	equ	b'000000'	;external RC network (default, inverted)
oschs3	equ	b'000001'	;high speed ext crystal/resonator (1MHz-75MHz)
oschs2	equ	b'000010'	;high speed ext crystal/resonator (1MHz-50MHz)
oschs1	equ	b'000011'	;high speed ext crystal/resonator (1MHz-50MHz)
oscxt2	equ	b'100000'	;normal ext crystal/resonator     (1MHz-24MHz)
oscxt1	equ	b'100001'	;normal ext crystal/resonator     (32KHz-10MHz)
osclp2	equ	b'100010'	;low power ext crystal/resonator  (32KHz-1MHz)
osclp1	equ	b'100011'	;low power ext crystal/resonator  (32KHz)

watchdog equ	1 << 2		;watchdog timer enabled
				; default to disabled
protect	equ	1 << 3		;code protect enabled (inverted)
				; default is to disable code protect

osc4mhz	equ	b'111' << 5	;internal 4MHz (inverted)
osc1mhz	equ	b'110' << 5	;internal 1MHz
osc128khz equ	b'101' << 5	;internal 128KHz
osc32khz equ	b'100' << 5	;internal 32KHz
				; default is internal RC oscillator disabled

ifb 	equ	1 << 6		;enable internal 1Mohm feedback (inverted)
				; default to disabled

sync	equ	1 << d'10'	;input syncing enabled (inverted)
				; default to disabled
turbo	equ	1 << d'11'	;turbo mode enabled (inverted)
				; default to disabled

pages4banks8	equ	b'00' << d'12'	;memory config (default, inverted)
pages4banks4	equ	b'01' << d'12'
pages2banks1	equ	b'10' << d'12'
pages1banks1	equ	b'11' << d'12'

bortrim0	equ	b'00' << d'14'	;brown-out trim bits (default)
bortrim1	equ	b'01' << d'14'
bortrim2	equ	b'10' << d'14'
bortrim3	equ	b'11' << d'14'

bor00		equ	b'00' << d'16'	;disabled (default, inverted)
bor22		equ	b'01' << d'16'	;2.2
bor26		equ	b'10' << d'16'	;2.6
bor42		equ	b'11' << d'16'	;4.2V brownout reset

carryx	equ	1 << d'18'	;ADDWF & SUBWF use carry input (inverted)
				; default is to ignore carry in

stackoptx equ	1 << d'19'	;extend stack->8 lev, option->8 bits (inverted)
				; default to 2 level stack, 6 bit option reg

;for modifying factory IRC calibration
trim0	equ	b'0000' << d'20'  ;highest frequency
trim3	equ	b'0001' << d'20'  ;
trim6	equ	b'0010' << d'20'  ;
trim9	equ	b'0011' << d'20'  ; about 3% per step
trim12	equ	b'1000' << d'20'  ;
trim15	equ	b'1001' << d'20'  ;
trim18	equ	b'1010' << d'20'  ;
trim21	equ	b'1011' << d'20'  ;lowest frequency (default, inverted)

pins18	equ	b'0' << d'22'	;default to 18 pin
pins28	equ	b'1' << d'22'

_invert	equ	0BF3FFBh

	else

;Define device symbols for configuration words (FUSE & FUSEX)
;per old version of SXDefs.inc 

oscrc	equ	b'00'		;external RC network (default, inverted)
oschs	equ	b'01'		;high speed external crystal/resonator
oscxt	equ	b'10'		;normal external crystal/resonator
osclp	equ	b'11'		;low power external crystal/resonator

watchdog equ	1 << 2		;watchdog timer enabled
				; default to disabled
protect	equ	1 << 3		;code protect enabled (inverted)
				; default is to disable code protect

osc4mhz	equ	b'1000' << 4	;internal 4MHz
osc2mhz	equ	b'1001' << 4	;internal 2MHz
osc1mhz	equ	b'1010' << 4	;internal 1MHz
osc500khz equ	b'1011' << 4	;internal 500KHz
osc250khz equ	b'1100' << 4	;internal 250KHz
osc125khz equ	b'1101' << 4	;internal 125KHz
osc62khz equ	b'1110' << 4	;internal 62.5KHz
osc31khz equ	b'1111' << 4	;internal 31.25KHz

stackx	equ	1 << d'8'	;stack is extended to 8 levels (inverted)
				; default to 2 levels
optionx	equ	1 << d'9'	;extend option register to 8 bits (inverted)
				; default to 6 bits
sync	equ	1 << d'10'	;input syncing enabled (inverted)
				; default to disabled
turbo	equ	1 << d'11'	;turbo mode enabled (inverted)
				; default to disabled

pages1	equ	b'00' << d'12'	;default
pages2	equ	b'01' << d'12'
pages4	equ	b'10' << d'12'
pages8	equ	b'11' << d'12'

banks1	equ	b'00' << d'14'	;default
banks2	equ	b'01' << d'14'
banks4	equ	b'10' << d'14'
banks8	equ	b'11' << d'14'

bor40	equ	b'11' << d'16'	;4.0V brownout reset
bor25	equ	b'10' << d'16'	;2.5
bor13	equ	b'01' << d'16'	;1.3
;bor00	equ	b'00' << d'16'	;disabled (default, inverted)

carryx	equ	1 << d'18'	;ADDWF & SUBWF use carry input (inverted)
				; default is to ignore carry in

pre7	equ	1 << d'19'	;for changing the preset FUSEX bit 7 (inverted)
				; default is no change

;for modifying factory IRC calibration
trim0	equ	b'0000' << d'20'  ;highest frequency
trim3	equ	b'0001' << d'20'  ;
trim6	equ	b'0010' << d'20'  ;
trim9	equ	b'0011' << d'20'  ; about 3% per step
trim12	equ	b'1000' << d'20'  ;
trim15	equ	b'1001' << d'20'  ;
trim18	equ	b'1010' << d'20'  ;
trim21	equ	b'1011' << d'20'  ;lowest frequency (default)

pins18	equ	b'0' << d'22'	;default to 18 pin
pins28	equ	b'1' << d'22'

_invert	equ	0F0F8Bh		;the default looks like a PIC16C54

	endif

_device	set	device ^ _invert
_fuse	set	_device & 0FFFh
_fusex	set	_device >> d'12'

_pins	set	(_fusex & 0400h) >> d'10'
_rom	set	_fusex & 003h

;Define macros for new Scenix instructions:

__NowBank set	0		;Default to bank 0 for both past and present
__LastBank set	0		; bank (like the H/W should but doesn't)
__NowPage set	7		;Default to top page for both
__LastPage set	7		; past and present page (like the H/W does)


;Define macro for ID label. Example:
;	id	'S', 'X', '-', 'D', 'e', 'm', 'o', ' '

id	macro	A,B,C,D,E,F,G,H		;set up ID label
	  nolist
	  local	_A,_B,_C,_D,_E,_F,_G,_H
_A	set	A
_B	set	B
_C	set	C
_D	set	D
_E	set	E
_F	set	F
_G	set	G
_H	set	H
	  errorlevel	-220	;don't display "address exceeds range" warning
	  org	1000h
	  data	(_A>>4)|0FF0h, A|0FF0h	;Unused bits are set
	  data	(_B>>4)|0FF0h, B|0FF0h
	  data	(_C>>4)|0FF0h, C|0FF0h
	  data	(_D>>4)|0FF0h, D|0FF0h
	  data	(_E>>4)|0FF0h, E|0FF0h
	  data	(_F>>4)|0FF0h, F|0FF0h
	  data	(_G>>4)|0FF0h, G|0FF0h
	  data	(_H>>4)|0FF0h, H|0FF0h
	  errorlevel	+220	;restore warning message
	  org	0
	  list
	endm

	errorlevel	-220	;don't display "address exceeds range" warning
	org	1010h
	list
	data	_fuse		;configuration bits (TURBO, SYNC, OPTIONX, etc.)
	data	_fusex		; (PINS, CARRYX, BOR40, BANKS, PAGES)
	nolist
	errorlevel	+220	;restore warning message
	org	0
	list
