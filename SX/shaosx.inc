; shaosx.inc - useful macros for SX by Alexander Shabarshin 
; E-mail: alexander@shabarshin.com
;
; 29-Oct-2006 : macros outpb, outp, inp, options, delay1, delay2
; 30-Oct-2006 : macros movlr, movrw, movir
; 05-Nov-2006 : macros movri, callw
; 14-Dec-2006 : macro movrr
; 16-Dec-2006 : comments for delay1 and delay2
; 25-Dec-2006 : macro nop3
; 28-Dec-2006 : macros bank0, bank1, bank2, bank3, bank4, bank5, bank6, bank7

; send byte to port
; outpb port-name,byte-value
outpb	macro	P,B ; 2
	movlw	B
	movwf	P
	endm
	
; send register value to port
; outp port-name,reg-name	
outp	macro	P,R ; 2
	movf	R,0
	movwf	P
	endm

; get byte from port and save it to register
; inp port-name,reg-name	
inp	macro	P,R ; 2
	movf	P,0
	movwf	R
	endm

; send byte to option
; options byte-value
options	macro	B ; 2
	movlw	B
	option
	endm	

; delay 4*N cycles with using one register
; delay1 byte-value,reg-name
delay1	macro	N,R1 ; 4*N (if N=0 then N=256)
	local	D1
	movlw	N
	movwf	R1
D1:	decfsz	R1,1	
	goto	D1
	endm

; delay	1028*N cycles with using two registers
; delay2 byte-value,reg-name-1,reg-name-2
delay2	macro	N,R1,R2 ; 1028*N (if N=0 then N=256)
	local	D2
        movlw	N
	movwf	R1
D2:	delay1	0,R2	
	decfsz	R1,1
	goto	D2	
	endm
	
; move byte to register
; movlr byte-value,reg-name
movlr	macro	B,R ; 2
	movlw	B
	movwf	R
	endm

; move register value to accumulator W
; movrw reg-name
movrw	macro	R ; 1
	movf	R,0
	endm

; move register value to register
; movrr reg-name,reg-name
movrr	macro	R1,R2; 2
	movf	R1,0
	movwf	R2
	endm

; move byte from [W] to register
; movir reg-name
movir	macro	R ; 3
	movwf	fsr
	movf	indf,0
	movwf	R
	endm

; move byte from register to [W]
; movri reg-name
movri	macro	R ; 3
	movwf	fsr
	movf	R,0
	movwf	indf
	endm

; set W and call subroutine
; callw byte-value,address
callw	macro	B,A ; 4
	movlw	B
	call	A
	endm

; spend 3 cycles in turbo mode by 1 instruction
nop3	macro	; 3
	local N3
    	goto N3
N3:
	endm
		
; set bank 0
bank0	macro	; 1	
	bank	0x00
	endm

; set bank 1
bank1	macro	; 1	
	bank	0x20
	endm

; set bank 2
bank2	macro	; 1	
	bank	0x40
	endm

; set bank 3
bank3	macro	; 1	
	bank	0x60
	endm

; set bank 4
bank4	macro	; 1	
	bank	0x80
	endm

; set bank 5
bank5	macro	; 1	
	bank	0xA0
	endm

; set bank 6
bank6	macro	; 1	
	bank	0xC0
	endm

; set bank 7
bank7	macro	; 1	
	bank	0xE0
	endm
