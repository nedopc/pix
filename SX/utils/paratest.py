#!/usr/bin/python

import parallel,time

def prn(p,i):
    while p.getInBusy():
	time.sleep(1e-9)
    p.setData(i)
    p.setDataStrobe(1)
    while not p.getInBusy():
	time.sleep(1e-9)
    p.setDataStrobe(0)

def crd(p,x,y):
    prn(p,1)
    prn(p,x)
    prn(p,2)
    prn(p,y)

def prc(p,c):
    prn(p,ord(c))

def clr(p,c):
    crd(p,0,0)
    i = 2000
    while i>0:
	prc(p,c)
	i=i-1

def prs(p,x,y,string):
    i = x
    crd(p,x,y)
    for c in string:
	if ord(c) == 0x09:
	    i=i+1
	    k=i&0xF8
	    if i&7:
		k=k+8
	    i = k
	    crd(p,i,y)
	if ord(c) >= 0x20:
	    prc(p,c)
	    i=i+1

def prf(p,filename):
    j = 0
    clr(p,' ')
    try:
        f = open(filename)
        for ll in f:
	    lll = ll.strip('\n')
	    if j==25:
		j = 0
		clr(p,' ')
	    prs(p,0,j,lll)
	    j=j+1
	f.close()
    except IOError:
	sys.stdout.write("File '"+filename+"' not exist!\r\n");


p = parallel.Parallel()
p.setDataStrobe(0)
#clr(p,' ')
prf(p,"text")
