; pixputer test 2

        processor 17C42A
	radix dec

include "p17c42a.inc"
    
include "pixmacro.inc"

delay10ms macro
	delay2	64,count1,count2
	clrwdt
	endm

delay5ms macro
	delay2	32,count1,count2
	clrwdt
	endm

delay2ms macro
	delay2	13,count1,count2
	clrwdt
	endm

delay1ms macro
	delay2	6,count1,count2
	clrwdt
	endm

delay100us macro
	delay1 165,count1
	clrwdt
	endm

delay2us macro
	delay1 2,count1
	clrwdt
	endm

; port A: 
; 0x04 - RS ("1" for data,"0" for instr)
; 0x08 - E ("1"->"0" for strobbing data)

disp_i	macro
	clrf	PORTA,f
	nop
	movlw	0x08
	movwf	PORTA
	nop
	nop
	nop
	clrf	PORTA,f
	endm
	
disp_d	macro
	movlw	0x04
	movwf	PORTA
	movlw	0x0C
	movwf	PORTA
	nop
	nop
	nop
	movlw	0x04
	movwf	PORTA
	endm	

count1	equ	0x1A	
count2	equ	0x1B
counter	equ	0x1C

	org 0x0000
_start:
	goto main

	org 0x0008
_int:
	retfie
    
	org 0x0010
_tmr0:
	retfie
    
	org 0x0018
_t0cki:
	retfie
    
	org 0x0020
_perif:
	retfie

; W - command
disp_comm:
	movwf	PORTB
	disp_i
	delay5ms
	return
	
; W - command
disp_data:
	movwf	PORTB
	disp_d
	delay1ms
	return		
    
disp_init:
	delay10ms
	delay10ms
	movlw	0x30
	movwf	PORTB
	disp_i
	delay5ms
	disp_i
	delay100us
	callw	0x38, disp_comm
	callw	0x08, disp_comm
	callw	0x01, disp_comm
	callw	0x06, disp_comm
	callw	0x0C, disp_comm
	return
	
main:
        clrf    ALUSTA,f	; clear ALUSTA
        bsf     ALUSTA,FS3      ; no auto increment FSR1
        bsf     ALUSTA,FS1      ; no auto increment FSR0
        bsf     CPUSTA,GLINTD   ; disable interrupts
	movlb   0		; bank 0
        clrf	PORTA,f		; clear port A
	bsf     PORTA,NOT_RBPU	; no weak pull ups for port B
	clrf	DDRB,f		; port B all outputs
	clrf	PORTB,f		; clear port B

	call	disp_init
	callw	'P', disp_data
	callw	'I', disp_data
	callw	'X', disp_data
	callw	'P', disp_data
	callw	'U', disp_data
	callw	'T', disp_data
	callw	'E', disp_data
	callw	'R', disp_data

loop:
	delay10ms
	goto	loop
    
	END
            