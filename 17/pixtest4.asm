; pixputer test 4

        processor 17C42A
	radix dec

include "p17c42a.inc"
    
include "pixmacro.inc"

RAMBAH	equ	0x40
ROMBAH	equ	0x60
REG3ADR	equ	0x37F3
RAMBADR equ	RAMBAH<<8
ROMBADR equ	ROMBAH<<8
BUFINIT equ	0x80

temp_w	equ	0x1A
temp_s	equ	0x1B
temp_b	equ	0x1C
temp_p	equ	0x1D
temp_f	equ	0x1E
iperif	equ	0x1F
count0	equ	0x20
count1	equ	0x21
count2	equ	0x22
counter	equ	0x23
testnum equ	0x24
hibyte	equ	0x25
bytel	equ	0x26
byteh	equ	0x27
tempch	equ	0x28

ibuf	equ	0x120
itmp	equ	0x121
iwhi	equ	0x122
iwlo	equ	0x123
ichi	equ	0x124
iclo	equ	0x125

delay1s	macro
	local	D1
	movlw	100
	movwf	counter
D1:	delay10ms
	decfsz	counter,f
	goto	D1
	endm

delay100ms macro
	local	D1
	movlw	10
	movwf	counter
D1:	delay10ms
	decfsz	counter,f
	goto	D1
	endm

delay10ms macro
	delay2	64,count1,count2
	clrwdt
	endm

delay5ms macro
	delay2	32,count1,count2
	clrwdt
	endm

delay2ms macro
	delay2	13,count1,count2
	clrwdt
	endm

delay1ms macro
	delay2	6,count1,count2
	clrwdt
	endm

delay100us macro
	delay1 165,count1
	clrwdt
	endm

delay2us macro
	delay1 2,count1
	clrwdt
	endm

	org 0x0000
_start:
	goto main
_t0001:	retlw	'0'
	data	0x4142
	data	0x4344
	data	0x4546
	data	0x4748
	data	0x494A
	data	0x4B4C

	org 0x0008
_int:	movpf	WREG,temp_w
	movpf	PCLATH,temp_p
	fcall	0x4008	; hack for handler in RAM
	movpf	temp_p,PCLATH
	movpf	temp_w,WREG
	retfie
    
	org 0x0010
_tmr0:	movpf	WREG,temp_w
	movpf	PCLATH,temp_p
	fcall	0x4010	; hack for handler in RAM
	movpf	temp_p,PCLATH
	movpf	temp_w,WREG
	retfie
    
	org 0x0018
_t0cki:	movpf	WREG,temp_w
	movpf	PCLATH,temp_p
	fcall	0x4018	; hack for handler in RAM
	movpf	temp_p,PCLATH
	movpf	temp_w,WREG
	retfie
    
	org 0x0020
_perif:	movpf	WREG,temp_w
	movpf	ALUSTA,temp_s
	movpf	BSR,temp_b
	movpf	FSR1,temp_f
	movpf	PCLATH,temp_p
	clrf	PCLATH,f
	; Autoincrement for FSR1
        bcf     ALUSTA,FS3 
	bsf	ALUSTA,FS2
	movlr	1
	movfp	ibuf^0x100,FSR1
	clrf	WREG,f
	cpfsgt	FSR1
	goto	__perif1
__perif0:
	clrwdt
	movlb	1
	btfss	PIR^0x100,RCIF
	goto	__perif1	
	movlb	0
	movpf	RCREG,iperif
	movlw	0x0A
	cpfseq	iperif
	goto	__perif01
	clrf	WREG,f
	movfp	WREG,INDF1
	movfp	WREG,FSR1
	goto	__perif1
__perif01:	
	movlw	0x1F
	cpfsgt	iperif
	goto	__perif0
	movfp	iperif,INDF1
	goto	__perif0
__perif1:	
	movpf	FSR1,ibuf^0x100
	movfp	temp_p,PCLATH
	movfp	temp_f,FSR1
	movfp	temp_w,WREG
	movfp	temp_s,ALUSTA
	movfp	temp_b,BSR
	retfie

; send character WREG to COM-port
com_send:
	clrwdt
	movlb	1
	btfss	PIR^0x100,TXIF
	goto	com_send
	movlb	0
	movwf	TXREG
	return

; port A: 
; 0x04 - RS ("1" for data,"0" for instr)
; 0x08 - E ("1"->"0" for strobbing data)

disp_i	macro
	clrf	PORTA,f
	nop
	movlw	0x08
	movwf	PORTA
	nop
	nop
	nop
	clrf	PORTA,f
	endm
	
disp_d	macro
	movlw	0x04
	movwf	PORTA
	movlw	0x0C
	movwf	PORTA
	nop
	nop
	nop
	movlw	0x04
	movwf	PORTA
	endm	

disp_clr macro
	callw	0x01, disp_comm
	endm
	
disp_1st macro
	callw	0x02, disp_comm
	endm
	
disp_2nd macro
	callw	0xC0, disp_comm
	endm		

disp_hexf macro R
	swapf	R,w
	call	disp_hex
	movfp	R,WREG
	call	disp_hex
	endm

; W - command
disp_comm:
	movwf	PORTB
	disp_i
	delay5ms
	return
	
; W - command
disp_data:
	movwf	PORTB
	disp_d
	delay1ms
	return		
    
disp_init:
	delay10ms
	delay10ms
	movlw	0x30
	movwf	PORTB
	disp_i
	delay5ms
	disp_i
	delay100us
	callw	0x38, disp_comm
	callw	0x08, disp_comm
	callw	0x01, disp_comm
	callw	0x06, disp_comm
	callw	0x0C, disp_comm
	return

; W - test number
disp_test:
	movwf	testnum
	disp_clr
	callw	'P', disp_data
	callw	'I', disp_data
	callw	'X', disp_data
	callw	'P', disp_data
	callw	'U', disp_data
	callw	'T', disp_data
	callw	'E', disp_data
	callw	'R', disp_data
	callw	' ', disp_data
	callw	'T', disp_data
	callw	'E', disp_data
	callw	'S', disp_data
	callw	'T', disp_data
	callw	'4', disp_data
	callw	'-', disp_data
	movfp	testnum,WREG
	addlw	0x30
	call	disp_data
	return
	
; W - print lower half byte
disp_hex:
	andlw	0x0F
	addlw	0x30
	movwf	testnum
	movlw	0x39
	cpfsgt	testnum
	goto	disp_hex_
	movfp	testnum,WREG
	addlw	0x07
	goto	disp_hex__
disp_hex_:
	movfp	testnum,WREG
disp_hex__:	
	call	disp_data
	return

big_delay:
	delay1s
	delay1s
	delay1s
	delay1s
	delay1s
	delay1s
	delay1s
	delay1s
	delay1s
	delay1s
	return

; read 1 hexadecimal digit using INDF0 and save to WREG
readh:	movlw	0x30
	subwf	INDF0,w
	movpf	WREG,itmp^0x100
	movlw	0x09
	cpfsgt	itmp^0x100
	goto	readh1
	movlw	0x07
	subwf	itmp^0x100,f	
readh1:	movfp	itmp^0x100,WREG
	return

; read 4 bytes using INDF0 and save 2-byte word (iwhi, iwlo)
readw:	call	readh
	movwf	iwhi^0x100
	swapf	iwhi^0x100,f
	call	readh
	addwf	iwhi^0x100,f
	call	readh
	movwf	iwlo^0x100
	swapf	iwlo^0x100,f
	call	readh
	addwf	iwlo^0x100,f
	return
	
; send half byte (WREG) in hexadecimal form
sendh:	andlw	0x0F
	addlw	0x30
	movwf	itmp^0x100
	movlw	0x39
	cpfsgt	itmp^0x100
	goto	sendh_
	movfp	itmp^0x100,WREG
	addlw	0x07
	goto	sendh__
sendh_:
	movfp	itmp^0x100,WREG
sendh__:	
	call	com_send
	return

; send 2-byte word (iwhi, iwlo) in hexadecimal form
sendw:	swapf	iwhi^0x100,w
	call	sendh
	movfp	iwhi^0x100,WREG
	call	sendh
	swapf	iwlo^0x100,w
	call	sendh
	movfp	iwlo^0x100,WREG
	call	sendh
	return
	
; send new line
sendn:	callw	0x0D,com_send
	callw	0x0A,com_send
	return	
	
; send ERR and new line
sende:	callw	'E',com_send
	callw	'R',com_send
	callw	'R',com_send
	callw	0x0D,com_send
	callw	0x0A,com_send
	return	

fakecall:
	movfp	iwhi^0x100,PCLATH
	movfp	iwlo^0x100,PCL
	
main:
        clrf    ALUSTA,f	; clear ALUSTA
        bsf     ALUSTA,FS3      ; no auto increment FSR1
        bcf     ALUSTA,FS1      ; auto increment FSR0
	bsf	ALUSTA,FS0
        bsf     CPUSTA,GLINTD   ; disable interrupts
	movlb   0		; bank 0
        clrf	PORTA,f		; clear port A
	bsf     PORTA,NOT_RBPU	; no weak pull ups for port B
	clrf	DDRB,f		; port B all outputs
	clrf	PORTB,f		; clear port B
	movlw	0x08		; enable peripheral interrupts
	movwf	INTSTA
	movlr	1		; register bank 1
	movlw	BUFINIT
	movwf	ibuf^0x100
	movlr	0		; register bank 0
	
	movlw	32 ; 9600 on 20 MHz
	movwf	SPBRG
	movlw	0x20
	movwf	TXSTA
	movlw	0x90
	movwf	RCSTA
	movlb	1
	movlw	0x01
	movwf	PIE^0x100
	movlb	0
        bcf     CPUSTA,GLINTD   ; enable interrupts

	call	disp_init
	callw	0,disp_test
	callw	'>',com_send
	
;	call	big_delay
;	disp_1st
;	callw	0x5C,disp_data

loop:	movlr	0
	delay10ms
	disp_2nd
	clrf	WREG,f
	movlr	1
	cpfseq	ibuf^0x100
	goto	loop1
	; command
	movlw	BUFINIT
	movwf	FSR0
	movpf	INDF0,itmp^0x100
	movlw	'?'
	cpfseq	itmp^0x100
	goto	loop01
	; command READ
	call	readw
	movfp	iwhi^0x100,WREG
	movwf	TBLPTRH
	movfp	iwlo^0x100,WREG
	movwf	TBLPTRL
	movpf	INDF0,itmp^0x100
	movlw	'='
	cpfseq	itmp^0x100
	goto	loop0r
	call	readw
	movfp	iwhi^0x100,WREG
	movwf	ichi^0x100
	movfp	iwlo^0x100,WREG
	movwf	iclo^0x100
	goto	loop0rr	
loop0r:	clrf	ichi^0x100,f
	movlw	0x01
	movwf	iclo^0x100
loop0rr:
	memr	iwhi^0x100
	movwf	iwlo^0x100
	call	sendw
	callw	' ',com_send
	tstfsz  iclo^0x100
	goto	loop0rrr
	decf    ichi^0x100,f
loop0rrr:	
	decf    iclo^0x100,f
	movfp	iclo^0x100,WREG
	iorwf	ichi^0x100,w
	btfss	ALUSTA,Z
	goto	loop0rr
	call	sendn
	goto	loop00
loop01: movlw	'!'
	cpfseq	itmp^0x100
	goto	loop02
	; command WRITE
	call	readw
	movfp	iwhi^0x100,WREG
	movwf	TBLPTRH
	movfp	iwlo^0x100,WREG
	movwf	TBLPTRL
	movpf	INDF0,itmp^0x100
	movlw	'='
	cpfseq	itmp^0x100
	goto	loop0w
loop0www:
	call	readw
	movfp	iwhi^0x100,WREG
	movwf	ichi^0x100
	movfp	iwlo^0x100,WREG
	movwf	iclo^0x100
	goto	loop0ww
loop0w:	decf	FSR0,f
	clrf	INDF0,f
	decf	FSR0,f
	clrf	ichi^0x100,f
	clrf	iclo^0x100,w
loop0ww:
	memw	ichi^0x100
	movpf	INDF0,WREG
	btfsc	ALUSTA,Z
	goto	loop00
	decf	FSR0,f
	goto	loop0www
loop02: movlw	'$'
	cpfseq	itmp^0x100
	goto	loop00
	; command GOTO
	call	readw
	call	fakecall
	goto	loop00
loop0e:	call	sende
loop00:	movlr	0
	callw	0,disp_test
	disp_2nd
	callw	'>',com_send	
	movlr	1
	movlw	BUFINIT
	movwf	ibuf^0x100
	goto	loop
loop1:	movlw	BUFINIT
	movpf	WREG,FSR0
loop2:	movfp	ibuf^0x100,WREG
	cpfslt	FSR0
	goto	loop
	movpf	INDF0,WREG
	movlr	0
	call	disp_data
	movlr	1
	goto	loop2
    
	END
            