; pixmacro.inc - useful macros for PIC17C4X
; ===============================================
; 10-Jan-2008 : dealy1,delay2
; 13-Jan-2008 : callw
; 19-Jan-2008 : memr,memrn,memrf,memw,memwn,memwf
; 20-Jan-2008 : memwd,fcall,fcallw
; 26-Jan-2008 : memr1,memr1n,memw1,memw1n,memw1d,fcallf
; 29-Jan-2008 : memr1f,memw1f,incwf,decwf,saveb,savew
; 30-Jan-2008 : addwfn,addwff,subwff,movff

; delay 3*N+3 cycles using one register (N=0 means 256)
; delay1 byte-value,reg-name
delay1	macro	N,R1
	local	D1
	movlw	N
	movwf	R1
D1:	decfsz	R1,1
	goto	D1
	endm
		    
; delay 774*N+3 cycles using two registers (N=0 means 256)
; delay2 byte-value,reg-name-1,reg-name-2
delay2	macro	N,R1,R2
	local	D2
	movlw	N
	movwf	R1
D2:	delay1	0,R2
	decfsz	R1,1
	goto	D2
	endm

; set W and call subroutine
; callw byte-value,address
callw	macro	B,A ; 3
	movlw	B
	call	A
	endm

; call far subroutine using numeric address
; fcall word-address
fcall	macro A ; 4
	movlw	HIGH(A)
	movwf	PCLATH
	lcall	LOW(A)
	endm

; set W and call far subroutine
; fcallw byte-value,word-address
fcallw	macro	B,A ; 5
	movlw	HIGH(A)
	movwf	PCLATH
	movlw	B
	lcall	LOW(A)
	endm

; set W from R and call far subroutine
; fcallf reg-input,word-address
fcallf	macro	R,A ; 5
	movlw	HIGH(A)
	movwf	PCLATH
	movfp	R,WREG
	lcall	LOW(A)
	endm

; read memory using TBLPTRH and TBLPTRL as address
; save lower byte to W and higher byte to register
; memr reg-hibyte
memr	macro	R ; 4
	tablrd	0,1,WREG
	tlrd	1,R
	tlrd	0,WREG
	endm
	
; read memory using numeric address
; save lower byte to W and higher byte to register
; memrn reg-hibyte,word-address
memrn	macro	R,A ; 8
	movlw	HIGH(A)
	movwf	TBLPTRH
	movlw	LOW(A)
	movwf	TBLPTRL
	memr	R
	endm
	
; read memory using address saved in two registers
; save lower byte to W and higher byte to register
; memrf reg-hibyte,reg-hiadr,reg-loadr
memrf	macro	R,RH,RL ; 8
	movfp	RH,WREG
	movwf	TBLPTRH
	movfp	RL,WREG
	movwf	TBLPTRL
	memr	R
	endm

; write memory using TBLPTRH and TBLPTRL as address
; get lower byte from W and higher byte from register
; memw reg-hibyte
memw	macro	R ; 3
	tlwt	1,R
	tablwt	0,1,WREG
	endm
	
; write memory using numeric address
; get lower byte from W and higher byte from register
; memwn reg-hibyte,word-address
memwn	macro	R,A ; 7
	tlwt	0,WREG
	movlw	HIGH(A)
	movwf	TBLPTRH
	movlw	LOW(A)
	movwf	TBLPTRL
	tablwt	1,1,R
	endm

; write memory using address saved in two registers
; get lower byte from W and higher byte from register
; memwf reg-hibyte,reg-hiadr,reg-loadr
memwf	macro	R,RH,RL ; 7
	tlwt	0,WREG
	movfp	RH,WREG
	movwf	TBLPTRH
	movfp	RL,WREG
	movwf	TBLPTRL
	tablwt	1,1,R
	endm

; write memory by numeric word using numeric address
; memwd word-data,word-address
memwd	macro	D,A ; 9
	movlw	HIGH(A)
	movwf	TBLPTRH
	movlw	LOW(A)
	movwf	TBLPTRL
	movlw	HIGH(D)
	tlwt	1,WREG
	movlw	LOW(D)
	tablwt	0,1,WREG
	endm
	
; read 1 byte from memory to W using TBLPTRH and TBLPTRL as address
; memr1
memr1	macro ; 3
	tablrd	0,1,WREG
	tlrd	0,WREG
	endm

; read 1 byte from memory to W using address saved in two registers
; memr1f reg-hiadr,reg-loadr
memr1f	macro	RH,RL ; 8
	movfp	RH,WREG
	movwf	TBLPTRH
	movfp	RL,WREG
	movwf	TBLPTRL
	memr1
	endm

; read 1 byte from memory to W using numeric address
; memr1n word-address
memr1n	macro	A ; 7
	movlw	HIGH(A)
	movwf	TBLPTRH
	movlw	LOW(A)
	movwf	TBLPTRL
	memr1
	endm
	
; write W to memory using TBLPTRH and TBLPTRL as address
; memw1
memw1	macro ; 2
	tablwt	0,1,WREG
	endm
	
; write W to memory using numeric address
; memw1n word-address
memw1n	macro	A ; 7
	tlwt	0,WREG
	movlw	HIGH(A)
	movwf	TBLPTRH
	movlw	LOW(A)
	movwf	TBLPTRL
	tablwt	1,1,WREG
	endm

; write W to memory using address saved in two registers
; memw1f reg-hiadr,reg-loadr
memw1f	macro	RH,RL ; 7
	tlwt	0,WREG
	movfp	RH,WREG
	movwf	TBLPTRH
	movfp	RL,WREG
	movwf	TBLPTRL
	tablwt	1,1,WREG
	endm
		
; write 1 byte to memory using numeric address
; memw1d byte-data,word-address
memw1d	macro	B,A ; 8
	movlw	B
	memw1n	A
	endm

; increment word saved in 2 registers
; incwf reg-hibyte,reg-lobyte
incwf	macro	RH,RL ; 2
	infsnz	RL,f
	incf	RH,f
	endm
	
; decrement word saved in 2 registers
; decwf reg-hibyte,reg-lobyte
decwf	macro	RH,RL ; 4
	local	L1
	tstfsz  RL
	goto	L1
	decf    RH,f
L1:	decf    RL,f
	endm

; save byte to register
; saveb byte-data,reg-data
saveb	macro	B,R ; 2
	movlw	B
	movwf	R
	endm

; save word to 2 registers	
; savew word-data,reg-hibyte,reg-lobyte
savew	macro	W,RH,RL ; 4
	movlw	HIGH(W)
	movwf	RH
	movlw	LOW(W)
	movwf	RL
	endm

; add constant to word saved in 2 registers
; addwn	reg-hibyte,reg-lobyte,word-data ; reg=reg+word
addwfn	macro	RH,RL,W ; 4
	movlw	LOW(W)
	addwf	RL,f
	movlw	HIGH(W)
	addwfc	RH,f
	endm
	
; add word saved in 2 registers to another word saved in other 2 registers
; addwf reg-hibyte,reg-lobyte,reg2-hibyte,reg2-lobyte ; reg=reg+reg2
addwff	macro	RH,RL,R2H,R2L ; 6
	movfp	R2H,WREG
	addwf	RH,f
	movfp	R2L,WREG
	addwf	RL,f
	btfsc	ALUSTA,C
	incf	RH,f
	endm
	
; subtruct word saved in 2 registers from another word saved in other 2 registers
; subwf reg-hibyte,reg-lobyte,reg2-hibyte,reg2-lobyte ; reg=reg-reg2
subwff	macro	RH,RL,R2H,R2L ; 6
	movfp	R2H,WREG
	subwf	RH,f
	movfp	R2L,WREG
	subwf	RL,f
	btfss	ALUSTA,C
	decf	RH,f
	endm

; move value of one register to another
; movff	reg-src,reg-dst
movff	macro	R1,R2 ; 2
	movfp	R1,WREG
	movwf	R2
	endm
