; pixputer RAM program 2 - test clock

	processor 17C42A
	radix dec
	    
include "p17c42a.inc"
include "pixmacro.inc"

disp_comm equ	0x004D
disp_data equ	0x0060
disp_hex  equ	0x00D6
big_delay equ	0x00E2

temp	equ	0xFF

disp_clr macro
	fcallw	0x01, disp_comm
	endm
	    
disp_1st macro
	fcallw	0x02, disp_comm
	endm
		    
disp_2nd macro
        fcallw	0xC0, disp_comm
	endm

disp_char macro B
	fcallw	B, disp_data
	endm
	
disp_hexf macro R
	swapf	R,f
	fcallf	R,disp_hex
	swapf	R,f
	fcallf	R,disp_hex
	endm

	org	0x4000

	disp_2nd
	disp_char '['
	memr1n	0x3004
	movwf	temp
	disp_hexf temp
	disp_char ':'
	memr1n	0x3002
	movwf	temp
	disp_hexf temp
	disp_char ']'
	disp_char ' '
	memr1n	0x3007
	movwf	temp
	disp_hexf temp
	disp_char '.'
	memr1n	0x3008
	movwf	temp
	disp_hexf temp
	disp_char '.'
	memr1n	0x3009
	movwf	temp
	disp_hexf temp

	fcall	big_delay

	return	
	
	END
