#!/usr/bin/python
import sys,intel_hex_format,serial

def reader():
    ss = ""
    while 1:
	b = s.read()
	if b=='\n':
	    break
	if b>=' ':
	    ss += b
    return ss.rstrip("\r")

def hex1(b):
    return ['0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F'][b]

def hex4(w):
    st = ""
    k3 = w>>12
    w -= k3<<12
    st += hex1(k3)
    k2 = w>>8
    w -= k2<<8
    st += hex1(k2)
    k1 = w>>4
    w -= k1<<4
    st += hex1(k1)
    k0 = w
    st += hex1(k0)
#    print st,k3,k2,k1,k0
    return st

port = 0
baudrate = 9600
adr = 0x4000
hexfile = intel_hex_format.IntelHexFile('pixram3.hex')
bytes = hexfile.toByteString(None,adr+adr)
bytelist = list(bytes)
h = 0
w = 0
wordlist = []
for b in bytelist:
    if(h==0):
	w = ord(b)
	h = 1
    else:
	w |= ord(b)<<8
	wordlist.append(w)
	h = 0
wordsize = len(wordlist)

try:
    s = serial.Serial(port, baudrate)
except:
    sys.stderr.write("Could not open port\n")
    sys.exit(1)

i = adr
h = 8
for w in wordlist:
    if h==8:
	outs = "!"+hex4(i)+"="
    outs += hex4(w)
    i+=1
    h-=1
    if h==0:
	h = 8
	s.write(outs+"\n")
	print s.read()
	outs = ""
if h!=8:
    s.write(outs+"\n")
    print s.read()

outs = "?"+hex4(adr)+"="+hex4(wordsize)+"\n"
#print outs
s.write(outs)
res = reader().strip(' ').split(' ')
rl = []
i = 0
e = 0
for x in res:
    if wordlist[i]!=int(x,16):
	print "ERROR in "+x
	e+=1
#    else:
#	print "OK in "+x
    i+=1
print s.read()
if e==0:
    print "Writing Ok - RUN"
    outs = "$"+hex4(adr)+"\n"
#    print outs
    s.write(outs)
else:
    print "Writing Failed - "+e+" ERRORS"
    	