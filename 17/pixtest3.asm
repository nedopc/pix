; pixputer test 3

        processor 17C42A
	radix dec

include "p17c42a.inc"
    
include "pixmacro.inc"

REG3ADR	equ	0x37F3
RAMBAH	equ	0x40
ROMBAH	equ	0x60

count0	equ	0x1A
count1	equ	0x1B	
count2	equ	0x1C
counter	equ	0x1D
testnum equ	0x1E
hibyte	equ	0x1F
bytel	equ	0x20
byteh	equ	0x21

delay1s	macro
	local	D1
	movlw	100
	movwf	counter
D1:	delay10ms
	decfsz	counter,f
	goto	D1
	endm

delay100ms macro
	local	D1
	movlw	10
	movwf	counter
D1:	delay10ms
	decfsz	counter,f
	goto	D1
	endm

delay10ms macro
	delay2	64,count1,count2
	clrwdt
	endm

delay5ms macro
	delay2	32,count1,count2
	clrwdt
	endm

delay2ms macro
	delay2	13,count1,count2
	clrwdt
	endm

delay1ms macro
	delay2	6,count1,count2
	clrwdt
	endm

delay100us macro
	delay1 165,count1
	clrwdt
	endm

delay2us macro
	delay1 2,count1
	clrwdt
	endm

	org 0x0000
_start:
	goto main
_t0001:	retlw	'0'
	data	0x4142
	data	0x4344
	data	0x4546
	data	0x4748
	data	0x494A
	data	0x4B4C

	org 0x0008
_int:
	retfie
    
	org 0x0010
_tmr0:
	retfie
    
	org 0x0018
_t0cki:
	retfie
    
	org 0x0020
_perif:
	retfie

; port A: 
; 0x04 - RS ("1" for data,"0" for instr)
; 0x08 - E ("1"->"0" for strobbing data)

disp_i	macro
	clrf	PORTA,f
	nop
	movlw	0x08
	movwf	PORTA
	nop
	nop
	nop
	clrf	PORTA,f
	endm
	
disp_d	macro
	movlw	0x04
	movwf	PORTA
	movlw	0x0C
	movwf	PORTA
	nop
	nop
	nop
	movlw	0x04
	movwf	PORTA
	endm	

disp_clr macro
	callw	0x01, disp_comm
	endm
	
disp_1st macro
	callw	0x02, disp_comm
	endm
	
disp_2nd macro
	callw	0xC0, disp_comm
	endm		

disp_hexf macro R
	swapf	R,w
	call	disp_hex
	movfp	R,WREG
	call	disp_hex
	endm

; W - command
disp_comm:
	movwf	PORTB
	disp_i
	delay5ms
	return
	
; W - command
disp_data:
	movwf	PORTB
	disp_d
	delay1ms
	return		
    
disp_init:
	delay10ms
	delay10ms
	movlw	0x30
	movwf	PORTB
	disp_i
	delay5ms
	disp_i
	delay100us
	callw	0x38, disp_comm
	callw	0x08, disp_comm
	callw	0x01, disp_comm
	callw	0x06, disp_comm
	callw	0x0C, disp_comm
	return

; W - test number
disp_test:
	movwf	testnum
	disp_clr
	callw	'P', disp_data
	callw	'I', disp_data
	callw	'X', disp_data
	callw	'P', disp_data
	callw	'U', disp_data
	callw	'T', disp_data
	callw	'E', disp_data
	callw	'R', disp_data
	callw	' ', disp_data
	callw	'T', disp_data
	callw	'E', disp_data
	callw	'S', disp_data
	callw	'T', disp_data
	callw	'3', disp_data
	callw	'-', disp_data
	movfp	testnum,WREG
	addlw	0x30
	call	disp_data
	return
	
; W - print lower half byte
disp_hex:
	andlw	0x0F
	addlw	0x30
	movwf	testnum
	movlw	0x39
	cpfsgt	testnum
	goto	disp_hex_
	movfp	testnum,WREG
	addlw	0x07
	goto	disp_hex__
disp_hex_:
	movfp	testnum,WREG
disp_hex__:	
	call	disp_data
	return

big_delay:
	delay1s
	delay1s
	delay1s
	delay1s
	delay1s
	delay1s
	delay1s
	delay1s
	delay1s
	delay1s
	return

	
main:
        clrf    ALUSTA,f	; clear ALUSTA
        bsf     ALUSTA,FS3      ; no auto increment FSR1
        bsf     ALUSTA,FS1      ; no auto increment FSR0
        bsf     CPUSTA,GLINTD   ; disable interrupts
	movlb   0		; bank 0
        clrf	PORTA,f		; clear port A
	bsf     PORTA,NOT_RBPU	; no weak pull ups for port B
	clrf	DDRB,f		; port B all outputs
	clrf	PORTB,f		; clear port B

	call	disp_init
test0:	
	callw	0,disp_test
	disp_2nd
	movlw	0
	movwf	TBLPTRL
	movwf	TBLPTRH

	tablrd	1,1,WREG
	tlrd	1,WREG
	call	disp_data ; 0
	tablrd	0,1,WREG
	call	disp_data ; 1
	tlrd	1,WREG
	call	disp_data ; 2
	tablrd	0,1,WREG
	call	disp_data ; 3
	tlrd	1,WREG
	call	disp_data ; 4
	tablrd	0,1,WREG
	call	disp_data ; 5
	tlrd	1,WREG
	call	disp_data ; 6
	tablrd	0,1,WREG
	call	disp_data ; 7
	tlrd	1,WREG
	call	disp_data ; 8
	tablrd	0,1,WREG
	call	disp_data ; 9
	tlrd	1,WREG
	call	disp_data ; A
	tablrd	0,1,WREG
	call	disp_data ; B
	tlrd	1,WREG
	call	disp_data ; C
	tablrd	0,1,WREG
	call	disp_data ; D
	tlrd	1,WREG
	call	disp_data ; E
	tlrd	0,WREG
	call	disp_data ; F

	call	big_delay

test1:
	callw	1,disp_test
	disp_2nd
	movlw	8
	movwf	count0
test1a:	
	movfp	count0,WREG
	sublw	8
	clrf	hibyte,f
;!!!	memwn	hibyte,REG3ADR
	memrn	hibyte,0x6000
	movwf	testnum
	movfp	hibyte,WREG
	call	disp_data
	movfp	testnum,WREG
	call	disp_data
	delay1s
	decfsz	count0,f
	goto	test1a

	call	big_delay
test2:
	callw	2,disp_test
	disp_2nd
	clrf	bytel,f
	clrf	byteh,f
	movlw	0x20
	movwf	count0
	clrf	TBLPTRL,f
	movlw	RAMBAH
	movwf	TBLPTRH
test2a:
	disp_2nd
	callw	'W', disp_data
	disp_hexf byteh
	disp_hexf bytel
	movfp	byteh,WREG	
	movwf	hibyte
	movfp	bytel,WREG
	memw	hibyte
	incf	bytel,f
	btfsc	ALUSTA,C
	incf	byteh,f
	movfp	byteh,WREG
	cpfseq	count0
	goto	test2a

	clrf	bytel,f
	clrf	byteh,f
	movlw	0x20
	movwf	count0
	clrf	TBLPTRL,f
	movlw	RAMBAH
	movwf	TBLPTRH
test2b:
	disp_2nd
	callw	'R', disp_data
	disp_hexf byteh
	disp_hexf bytel
	memr	hibyte
	cpfseq	bytel
	goto	test2err
	movfp	hibyte,WREG
	cpfseq	byteh
	goto	test2err
	incf	bytel,f
	btfsc	ALUSTA,C
	incf	byteh,f
	movfp	byteh,WREG
	cpfseq	count0
	goto	test2b
	goto	test3
	
test2err:
	callw	'-', disp_data
	disp_hexf hibyte	
	tlrd	0,hibyte
	disp_hexf hibyte
	call	big_delay
	
test3:	
	callw	3,disp_test
	disp_2nd
	movlw	8
	movwf	count0
test3a:	
	movfp	count0,WREG
	sublw	8
	clrf	hibyte,f
;!!!	memwn	hibyte,REG3ADR
	fcall	0x6001
	call	disp_data
	delay1s
	decfsz	count0,f
	goto	test3a
	memwd	0xB638,0x4001
	fcall	0x4001
	call	disp_data
	
	call	big_delay
		
	disp_1st
	callw	0x5C,disp_data

loop:
	delay10ms
	goto	loop
    
	org	0x2000
	data	0x3031
	retlw	'1'
	org	0x4000
	data	0x3032
	retlw	'2'
	org	0x6000
	data	0x3033
	retlw	'3'
    
	END
            