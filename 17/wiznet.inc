; WizNET definitions
COMMON_BASE	equ 0x8000 ; out base address
IINCHIP_MAP_TXBUF equ COMMON_BASE + 0x4000
IINCHIP_MAP_RXBUF equ COMMON_BASE + 0x6000
MAX_SOCK_NUM	equ 4
SOCK0		equ 0
SOCK1		equ 1
SOCK2		equ 2
SOCK3		equ 3
MR		equ COMMON_BASE
; Gateway IP Register address
GAR0		equ (COMMON_BASE + 0x0001)
; Subnet mask Register address
SUBR0		equ (COMMON_BASE + 0x0005)
; Source MAC Register address
SHAR0		equ (COMMON_BASE + 0x0009)
; Source IP Register address
SIPR0		equ (COMMON_BASE + 0x000F)
; Interrupt Register
IR		equ (COMMON_BASE + 0x0015)
; Interrupt mask register
IMR		equ (COMMON_BASE + 0x0016)
; Timeout register address( 1 is 100us )
RTR0		equ (COMMON_BASE + 0x0017)
; Retry count reigster
RCR		equ (COMMON_BASE + 0x0019)
; Receive memory size reigster
RMSR		equ (COMMON_BASE + 0x001A)
; Transmit memory size reigster
TMSR		equ (COMMON_BASE + 0x001B)
; Unreachable IP register address in UDP mode
UIPR0		equ (COMMON_BASE + 0x002A)
; Unreachable Port register address in UDP mode
UPORT0		equ (COMMON_BASE + 0x002E)
; socket register
CH_BASE		equ (COMMON_BASE + 0x0400)
; size of each channel register map
CH_SIZE		equ 0x0100
; socket Mode registers
S0_MR		equ (CH_BASE + 0 * CH_SIZE + 0x0000)
S1_MR		equ (CH_BASE + 1 * CH_SIZE + 0x0000)
S2_MR		equ (CH_BASE + 2 * CH_SIZE + 0x0000)
S3_MR		equ (CH_BASE + 3 * CH_SIZE + 0x0000)
; channel Sn_CR registers
S0_CR		equ (CH_BASE + 0 * CH_SIZE + 0x0001)
S1_CR		equ (CH_BASE + 1 * CH_SIZE + 0x0001)
S2_CR		equ (CH_BASE + 2 * CH_SIZE + 0x0001)
S3_CR		equ (CH_BASE + 3 * CH_SIZE + 0x0001)
; channel interrupt registers
S0_IR		equ (CH_BASE + 0 * CH_SIZE + 0x0002)
S1_IR		equ (CH_BASE + 1 * CH_SIZE + 0x0002)
S2_IR		equ (CH_BASE + 2 * CH_SIZE + 0x0002)
S3_IR		equ (CH_BASE + 3 * CH_SIZE + 0x0002)
; channel status registers
S0_SR		equ (CH_BASE + 0 * CH_SIZE + 0x0003)
S1_SR		equ (CH_BASE + 1 * CH_SIZE + 0x0003)
S2_SR		equ (CH_BASE + 2 * CH_SIZE + 0x0003)
S3_SR		equ (CH_BASE + 3 * CH_SIZE + 0x0003)
; source port registers
S0_PORT0	equ (CH_BASE + 0 * CH_SIZE + 0x0004)
S1_PORT0	equ (CH_BASE + 1 * CH_SIZE + 0x0004)
S2_PORT0	equ (CH_BASE + 2 * CH_SIZE + 0x0004)
S3_PORT0	equ (CH_BASE + 3 * CH_SIZE + 0x0004)
; Peer MAC registers
S0_DHAR0	equ (CH_BASE + 0 * CH_SIZE + 0x0006)
S1_DHAR0	equ (CH_BASE + 1 * CH_SIZE + 0x0006)
S2_DHAR0	equ (CH_BASE + 2 * CH_SIZE + 0x0006)
S3_DHAR0	equ (CH_BASE + 3 * CH_SIZE + 0x0006)
; Peer IP registers
S0_DIPR0	equ (CH_BASE + 0 * CH_SIZE + 0x000C)
S1_DIPR0	equ (CH_BASE + 1 * CH_SIZE + 0x000C)
S2_DIPR0	equ (CH_BASE + 2 * CH_SIZE + 0x000C)
S3_DIPR0	equ (CH_BASE + 3 * CH_SIZE + 0x000C)
; Peer port registers
S0_DPORT0	equ (CH_BASE + 0 * CH_SIZE + 0x0010)
S1_DPORT0	equ (CH_BASE + 1 * CH_SIZE + 0x0010)
S2_DPORT0	equ (CH_BASE + 2 * CH_SIZE + 0x0010)
S3_DPORT0	equ (CH_BASE + 3 * CH_SIZE + 0x0010)
; Maximum Segment Size(Sn_MSSR0) registers
S0_MSSR0	equ (CH_BASE + 0 * CH_SIZE + 0x0012)
S1_MSSR1	equ (CH_BASE + 1 * CH_SIZE + 0x0012)
S2_MSSR2	equ (CH_BASE + 2 * CH_SIZE + 0x0012)
S3_MSSR3	equ (CH_BASE + 3 * CH_SIZE + 0x0012)
; Protocol of IP Header field registers in IP raw mode
S0_PROTO	equ (CH_BASE + 0 * CH_SIZE + 0x0014)
S1_PROTO	equ (CH_BASE + 1 * CH_SIZE + 0x0014)
S2_PROTO	equ (CH_BASE + 2 * CH_SIZE + 0x0014)
S3_PROTO	equ (CH_BASE + 3 * CH_SIZE + 0x0014)
; IP Type of Service(TOS) Registers
S0_TOS		equ (CH_BASE + 0 * CH_SIZE + 0x0015)
S1_TOS		equ (CH_BASE + 1 * CH_SIZE + 0x0015)
S2_TOS		equ (CH_BASE + 2 * CH_SIZE + 0x0015)
S3_TOS		equ (CH_BASE + 3 * CH_SIZE + 0x0015)
; IP Time to live(TTL) Registers
S0_TTL		equ (CH_BASE + 0 * CH_SIZE + 0x0016)
S1_TTL		equ (CH_BASE + 1 * CH_SIZE + 0x0016)
S2_TTL		equ (CH_BASE + 2 * CH_SIZE + 0x0016)
S3_TTL		equ (CH_BASE + 3 * CH_SIZE + 0x0016)
; Transmit free memory size registers
S0_TX_FSR0	equ (CH_BASE + 0 * CH_SIZE + 0x0020)
S1_TX_FSR0	equ (CH_BASE + 1 * CH_SIZE + 0x0020)
S2_TX_FSR0	equ (CH_BASE + 2 * CH_SIZE + 0x0020)
S3_TX_FSR0	equ (CH_BASE + 3 * CH_SIZE + 0x0020)
; Transmit memory read pointer registers
S0_TX_RD0	equ (CH_BASE + 0 * CH_SIZE + 0x0022)
S1_TX_RD0	equ (CH_BASE + 1 * CH_SIZE + 0x0022)
S2_TX_RD0	equ (CH_BASE + 2 * CH_SIZE + 0x0022)
S3_TX_RD0	equ (CH_BASE + 3 * CH_SIZE + 0x0022)
; Transmit memory write pointer registers
S0_TX_WR0	equ (CH_BASE + 0 * CH_SIZE + 0x0024)
S1_TX_WR0	equ (CH_BASE + 1 * CH_SIZE + 0x0024)
S2_TX_WR0	equ (CH_BASE + 2 * CH_SIZE + 0x0024)
S3_TX_WR0	equ (CH_BASE + 3 * CH_SIZE + 0x0024)
; Received data size registers
S0_RX_RSR0	equ (CH_BASE + 0 * CH_SIZE + 0x0026)
S1_RX_RSR0	equ (CH_BASE + 1 * CH_SIZE + 0x0026)
S2_RX_RSR0	equ (CH_BASE + 2 * CH_SIZE + 0x0026)
S3_RX_RSR0	equ (CH_BASE + 3 * CH_SIZE + 0x0026)
; Read point of Receive memory
S0_RX_RD0	equ (CH_BASE + 0 * CH_SIZE + 0x0028)
S1_RX_RD0	equ (CH_BASE + 1 * CH_SIZE + 0x0028)
S2_RX_RD0	equ (CH_BASE + 2 * CH_SIZE + 0x0028)
S3_RX_RD0	equ (CH_BASE + 3 * CH_SIZE + 0x0028)
; Write point of Receive memory
S0_RX_WR0	equ (CH_BASE + 0 * CH_SIZE + 0x002A)
S1_RX_WR0	equ (CH_BASE + 1 * CH_SIZE + 0x002A)
S2_RX_WR0	equ (CH_BASE + 2 * CH_SIZE + 0x002A)
S3_RX_WR0	equ (CH_BASE + 3 * CH_SIZE + 0x002A)
    
; MODE register values
MR_RST		equ 0x80 ; reset
MR_PB		equ 0x10 ; ping block 
MR_PPPOE	equ 0x08 ; enable pppoe 
MR_LB  		equ 0x04 ; little or big endian selector in indirect mode 
MR_AI		equ 0x02 ; auto-increment in indirect mode 
MR_IND		equ 0x01 ; enable indirect mode 

; IR register values 
IR_CONFLICT	equ 0x80 ; check ip confict 
IR_UNREACH	equ 0x40 ; get the destination unreachable message in UDP sending 
IR_PPPoE	equ 0x20 ; get the PPPoE close message 
IR_SOCK0	equ 0x01 ; check socket 0 interrupt 
IR_SOCK1	equ 0x02 ; check socket 1 interrupt 
IR_SOCK2	equ 0x04 ; check socket 2 interrupt 
IR_SOCK3	equ 0x08 ; check socket 3 interrupt 
    
; Sn_MR values 
Sn_MR_CLOSE	equ 0x00 ; unused socket 
Sn_MR_TCP	equ 0x01 ; TCP 
Sn_MR_UDP	equ 0x02 ; UDP 
Sn_MR_IPRAW	equ 0x03 ; IP LAYER RAW SOCK 
Sn_MR_MACRAW	equ 0x04 ; MAC LAYER RAW SOCK 
Sn_MR_PPPOE	equ 0x05 ; PPPoE 
Sn_MR_ND	equ 0x20 ; No Delayed Ack(TCP) flag 
Sn_MR_MULTI	equ 0x80 ; support multicating 

; Sn_CR values 
Sn_CR_OPEN	equ 0x01 ; initialize or open socket 
Sn_CR_LISTEN	equ 0x02 ; wait connection request in tcp mode(Server mode) 
Sn_CR_CONNECT	equ 0x04 ; send connection request in tcp mode(Client mode) 
Sn_CR_DISCON	equ 0x08 ; send closing reqeuset in tcp mode 
Sn_CR_CLOSE	equ 0x10 ; close socket 
Sn_CR_SEND	equ 0x20 ; updata txbuf pointer, send data 
Sn_CR_SEND_MAC	equ 0x21 ; send data with MAC address, so without ARP process 
Sn_CR_SEND_KEEP	equ 0x22 ; send keep alive message 
Sn_CR_RECV	equ 0x40 ; update rxbuf pointer, recv data 

; Sn_IR values 
Sn_IR_SEND_OK	equ 0x10 ; complete sending 
Sn_IR_TIMEOUT	equ 0x08 ; assert timeout 
Sn_IR_RECV	equ 0x04 ; receiving data 
Sn_IR_DISCON	equ 0x02 ; closed socket 
Sn_IR_CON	equ 0x01 ; established connection 

; Sn_SR values 
SOCK_CLOSED	equ 0x00 ; closed 
SOCK_INIT 	equ 0x13 ; init state 
SOCK_LISTEN	equ 0x14 ; listen state 
SOCK_SYNSENT	equ 0x15 ; connection state 
SOCK_SYNRECV	equ 0x16 ; connection state 
SOCK_ESTABLISHED equ 0x17 ; success to connect 
SOCK_FIN_WAIT	equ 0x18 ; closing state 
SOCK_CLOSING	equ 0x1A ; closing state 
SOCK_TIME_WAIT	equ 0x1B ; closing state 
SOCK_CLOSE_WAIT	equ 0x1C ; closing state 
SOCK_LAST_ACK	equ 0x1D ; closing state 
SOCK_UDP	equ 0x22 ; udp socket 
SOCK_IPRAW	equ 0x32 ; ip raw mode socket 
SOCK_MACRAW	equ 0x42 ; mac raw mode socket 
SOCK_PPPOE	equ 0x5F ; pppoe socket 

; IP PROTOCOL 
IPPROTO_IP	equ 0    ; Dummy for IP 
IPPROTO_ICMP    equ 1    ; Control message protocol 
IPPROTO_IGMP    equ 2    ; Internet group management protocol 
IPPROTO_GGP     equ 3    ; Gateway^2 (deprecated) 
IPPROTO_TCP     equ 6    ; TCP 
IPPROTO_PUP     equ 12   ; PUP 
IPPROTO_UDP     equ 17   ; UDP 
IPPROTO_IDP     equ 22   ; XNS idp 
IPPROTO_ND      equ 77   ; UNOFFICIAL net disk protocol 
IPPROTO_RAW     equ 255  ; Raw IP packet 

