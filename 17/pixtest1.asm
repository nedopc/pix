; pixputer test 1

        processor 17C42A
	radix dec

include "p17c42a.inc"
    
include "pixmacro.inc"

count1	equ	0x1A	
count2	equ	0x1B
counter	equ	0x1C

	org 0x0000
_start:
	goto main

	org 0x0008
_int:
	retfie
    
	org 0x0010
_tmr0:
	retfie
    
	org 0x0018
_t0cki:
	retfie
    
	org 0x0020
_perif:
	retfie
    
main:
        clrf    ALUSTA,f	; clear ALUSTA
        bsf     ALUSTA,FS3      ; no auto increment FSR1
        bsf     ALUSTA,FS1      ; no auto increment FSR0
        bsf     CPUSTA,GLINTD   ; disable interupts
	movlb   0		; bank 0
        clrf    PORTA,f		; clear port A
	bsf     PORTA,NOT_RBPU	; no weak pull ups for port B
	clrf	DDRB,f
	clrf	counter,f
loop:	
	comf	counter,w
	movwf	PORTB
	delay2	64,count1,count2
	clrwdt
	delay2	64,count1,count2
	clrwdt
	delay2	64,count1,count2
	clrwdt
	delay2	64,count1,count2
	clrwdt
	incf	counter,f
	goto	loop
    
	END
            