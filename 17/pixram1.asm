; pixputer RAM program 1 - test display

	processor 17C42A
	radix dec
	    
include "p17c42a.inc"
include "pixmacro.inc"

disp_comm equ	0x004D
disp_data equ	0x0060
disp_hex  equ	0x00D6
big_delay equ	0x00E2

disp_clr macro
	fcallw	0x01, disp_comm
	endm
	    
disp_1st macro
	fcallw	0x02, disp_comm
	endm
		    
disp_2nd macro
        fcallw	0xC0, disp_comm
	endm

disp_char macro B
	fcallw	B, disp_data
	endm
	
disp_hexf macro R
	swapf	R,f
	fcallf	R,disp_hex
	swapf	R,f
	fcallf	R,disp_hex
	endm

	org	0x4000

	disp_2nd
	disp_char	'H'
	disp_char	'E'
	disp_char	'L'
	disp_char	'L'
	disp_char	'O'
	disp_char	' '
	
	movlw	0x9A
	movwf	0xFF
	disp_hexf 0xFF

	fcall	big_delay

	return	
	
	END
