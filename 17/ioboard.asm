;    PIXPUTER firmware (contest version for project #001098)
;
;    Copyright (C) 2008, Alexander A. Shabarshin <ashabarshin@gmail.com>
;
;    This program is free software: you can redistribute it and/or modify
;    it under the terms of the GNU General Public License as published by
;    the Free Software Foundation, either version 3 of the License, or
;    (at your option) any later version.
;
;    This program is distributed in the hope that it will be useful,
;    but WITHOUT ANY WARRANTY; without even the implied warranty of
;    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;    GNU General Public License for more details.
;
;    You should have received a copy of the GNU General Public License
;    along with this program.  If not, see <http://www.gnu.org/licenses/>.
;
        processor 17C42A
	radix dec

include "p17c42a.inc"
include "pixmacro.inc"
include "wiznet.inc"

RAMBAH	equ	0x40
ROMBAH	equ	0x60
REG3ADR	equ	0x37F3
RAMBADR equ	RAMBAH<<8
ROMBADR equ	ROMBAH<<8
NVMPHI	equ	0x300E
NVMPLO	equ	0x300F
NVMINET	equ	0x3011
WIZINET	equ	0x8001	 
PAGSIZE	equ	0x2000
BUFINIT equ	0x80
BUFSIZE equ	0x80

; RMSR = 0x03
RXSIZE	equ	8192
RXMASK	equ	0x1FFF
; TMSR = 0x03
TXSIZE	equ	8192
TXMASK	equ	0x1FFF

temp_w	equ	0x1A
temp_s	equ	0x1B
temp_b	equ	0x1C
temp_p	equ	0x1D
temp_f	equ	0x1E
iperif	equ	0x1F
count0	equ	0x20
count1	equ	0x21
count2	equ	0x22
counter	equ	0x23
testnum equ	0x24
hibyte	equ	0x25
bytel	equ	0x26
byteh	equ	0x27
tempch	equ	0x28

stat1	equ	0x34
stat2	equ	0x35
isqwe	equ	0x36
isqwe1	equ	0x37
isqwe2	equ	0x38
isqwe3	equ	0x39
dstahi	equ	0x3A
dstalo	equ	0x3B
temp3	equ	0x3C
temp4	equ	0x3D
temp5	equ	0x3E
tmphex	equ	0x3F
fzero	equ	0x40
temp	equ	0x41
asrchi	equ	0x42
asrclo	equ	0x43
adsthi	equ	0x44
adstlo	equ	0x45
ncount	equ	0x46
nerr	equ	0x47
rtemp	equ	0x48
htemp	equ	0x49
gethi	equ	0x4A
getlo	equ	0x4B
qtemp	equ	0x4C
counta  equ	0x4D
countb	equ	0x4E
countc	equ	0x4F
counth	equ	0x50
countl	equ	0x51
hibyte2	equ	0x52
stemp	equ	0x53
curhi	equ	0x54
curlo	equ	0x55
temp_h	equ	0x56
temp_l	equ	0x57
freeh	equ	0x58
freel	equ	0x59
temp1	equ	0x5A
temp2	equ	0x5B
stahi	equ	0x5C
stalo	equ	0x5D
wrhi	equ	0x5E
wrlo	equ	0x5F
itemp	equ	0x60
ireg	equ	0x61
iadrhi	equ	0x62
iadrlo	equ	0x63
isizhi	equ	0x64
isizlo	equ	0x65
ioffhi	equ	0x66
iofflo	equ	0x67
istahi	equ	0x68
istalo	equ	0x69
irdhi	equ	0x6A
irdlo	equ	0x6B
itmphi	equ	0x6C
itmplo	equ	0x6D
ilefhi	equ	0x6E
ileflo	equ	0x6F
iflag	equ	0x70
iregg	equ	0x71

ibuf	equ	0x120
itmp	equ	0x121
iwhi	equ	0x122
iwlo	equ	0x123
ichi	equ	0x124
iclo	equ	0x125

delay1s	macro
	local	D1
	movlw	100
	movwf	counter
D1:	delay10ms
	decfsz	counter,f
	goto	D1
	endm

delay100ms macro
	local	D1
	movlw	10
	movwf	counter
D1:	delay10ms
	decfsz	counter,f
	goto	D1
	endm

delay10ms macro
	delay2	64,count1,count2
	clrwdt
	endm

memtst	macro	A,V,L
	memr1n	A
	movwf	temp
	movlw	V
	cpfseq	temp
	goto	L
	endm

	org 0x0000
_start:
	goto main

	org 0x0008
_int:	movpf	WREG,temp_w
	movpf	PCLATH,temp_p
	call	exint
	movpf	temp_p,PCLATH
	movpf	temp_w,WREG
	retfie
    
	org 0x0010
	retfie
    
	org 0x0018
_t0cki:	movpf	WREG,temp_w
	movpf	PCLATH,temp_p
	call	extoki
	movpf	temp_p,PCLATH
	movpf	temp_w,WREG
	retfie
    
	org 0x0020
_perif:	movpf	WREG,temp_w
	movpf	ALUSTA,temp_s
	movpf	BSR,temp_b
	movpf	FSR1,temp_f
	movpf	PCLATH,temp_p
	clrf	PCLATH,f
	; Autoincrement for FSR1
        bcf     ALUSTA,FS3 
	bsf	ALUSTA,FS2
	movlr	1
	movfp	ibuf^0x100,FSR1
	clrf	WREG,f
	cpfsgt	FSR1
	goto	__perif1
__perif0:
	clrwdt
	movlb	1
	btfss	PIR^0x100,RCIF
	goto	__perif1	
	movlb	0
	movpf	RCREG,iperif
	movlw	0x0A
	cpfseq	iperif
	goto	__perif01
	clrf	WREG,f
	movfp	WREG,INDF1
	movfp	WREG,FSR1
	goto	__perif1
__perif01:	
	movlw	0x1F
	cpfsgt	iperif
	goto	__perif0
	movfp	iperif,INDF1
	goto	__perif0
__perif1:	
	movpf	FSR1,ibuf^0x100
	movfp	temp_p,PCLATH
	movfp	temp_f,FSR1
	movfp	temp_w,WREG
	movfp	temp_s,ALUSTA
	movfp	temp_b,BSR
	retfie

extoki:	movpf	ALUSTA,temp_s
	movpf	BSR,temp_b
	movlr	0
	movpf	TBLPTRH,temp_h
	movpf	TBLPTRL,temp_l
	; increment counter and pass every 2nd interrupt
	incf	isqwe,f
	btfsc	isqwe,0
	goto	sqwe0
	movlw	HIGH(ROMBADR)
	cpfseq	curhi
	goto	sqwe1
	movlw	LOW(ROMBADR)
	cpfseq	curlo
	goto	sqwe1
	savew	RAMBADR,curhi,curlo
sqwe1:	memr1n	0x3009 ; read year
	movwf	isqwe3
	memr1n	0x3008 ; read month
	memwf	isqwe3,curhi,curlo
	incwf	curhi,curlo
	memr1n	0x3007 ; read date
	movwf	isqwe3
	memr1n	0x3004 ; read hours
	memwf	isqwe3,curhi,curlo
	incwf	curhi,curlo
	memr1n	0x3002 ; read minutes
	movwf	isqwe3
	memr1n	0x3000 ; read seconds
	memwf	isqwe3,curhi,curlo
	incwf	curhi,curlo
	movpf	PORTB,isqwe3
	clrf	WREG,f
	tstfsz	stat1
	iorlw	0x01
	tstfsz	stat2
	iorlw	0x02
	memwf	isqwe3,curhi,curlo
	incwf	curhi,curlo
sqwe0:	movfp	temp_l,TBLPTRL
	movfp	temp_h,TBLPTRH
	movfp	temp_b,BSR	
	movfp	temp_s,ALUSTA
	return ; retfie above
	
exint:	movpf	ALUSTA,temp_s
	movpf	BSR,temp_b
	movpf	FSR1,temp_f
	movlr	0
	movpf	TBLPTRH,temp_h
	movpf	TBLPTRL,temp_l
	; Autoincrement for FSR1
        bcf     ALUSTA,FS3 
	bsf	ALUSTA,FS2
	; check nature of interrupt
	memr1n	IR
	movwf	iregg
	btfss	iregg,0
	goto	intend
	; read interrupt register for socket 0
	memr1n	S0_IR
	movwf	ireg
	; check for RECV interruption
	btfss	ireg,2
	goto	intend
	; get out if buffer still busy 
	saveb	BUFINIT,FSR1
	; get received size, also save it as left size
	memr1n	S0_RX_RSR0
	movwf	isizhi
	movwf	ilefhi
	memr1n	S0_RX_RSR0+1
	movwf	isizlo
	movwf	ileflo
	; calculate offset and save RD-address for future 
	memr1n	S0_RX_RD0
	movwf	irdhi
	andlw	HIGH(RXMASK)
	movwf	ioffhi
	memr1n	S0_RX_RD0+1
	movwf	irdlo
	andlw	LOW(RXMASK)
	movwf	iofflo
	; calculate start address
	savew	IINCHIP_MAP_RXBUF,istahi,istalo
	addwff	istahi,istalo,ioffhi,iofflo
	; save threshold for buffer
	savew	IINCHIP_MAP_RXBUF+RXSIZE,itmphi,itmplo
inorm:	memr1f	istahi,istalo
	movwf	INDF1
	decwf	ilefhi,ileflo
	; check for end of data
	movfp	ilefhi,WREG
	iorwf	ileflo,w
	btfsc	ALUSTA,Z
	goto	irdok
	incwf	istahi,istalo
	; check for buffer overflow
	movfp	istahi,WREG
	cpfseq	itmphi
	goto	inorm2
	movfp	istalo,WREG
	cpfseq	itmplo
	goto	inorm2
	; buffer overflow
	savew	IINCHIP_MAP_RXBUF,istahi,istalo
inorm2:	; check for end of our buffer
	tstfsz	FSR1
	goto	inorm
irdok:	; our buffer filled
	addwff	irdhi,irdlo,isizhi,isizlo
	subwff	irdhi,irdlo,ilefhi,ileflo
	movfp	irdhi,WREG
	memw1n	S0_RX_RD0
	movfp	irdlo,WREG
	memw1n	S0_RX_RD0+1
	memw1d	Sn_CR_RECV,S0_CR ; should we do this here? we got everything already...
	; signal for main program
	saveb	1,iflag 
	; disable socket 0 interrupts
	memw1d	0x00,IMR ; ???
intend:	movfp	ireg,WREG
	andlw	0x0F
	memw1n	S0_IR
	movfp	iregg,WREG
	memw1n	IR
	movfp	temp_l,TBLPTRL
	movfp	temp_h,TBLPTRH
	movfp	temp_f,FSR1
	movfp	temp_b,BSR	
	movfp	temp_s,ALUSTA
	return ; retfie see above

; send buffer through WizNET (interrupts must be disabled!)
sendbuf:
	movlw	BUFINIT
	subwf	FSR0,w
	movwf	countl
	movwf	countc
	clrf	counth,f
sendbuf0:	
	; get free size
	memr1n	S0_TX_FSR0
	movwf	freeh
	memr1n	S0_TX_FSR0+1
	movwf	freel
	saveb	BUFINIT,FSR0
	clrwdt
	movff	freeh,temp1
	movff	freel,temp2
	subwff	temp1,temp2,counth,countl
	btfsc	temp1,7
	goto	sendbuf0
	; calculate offset and save WR-address for future
	memr1n	S0_TX_WR0
	movwf	wrhi
	andlw	HIGH(TXMASK)
	movwf	temp1
	memr1n	S0_TX_WR0+1
	movwf	wrlo
	andlw	LOW(TXMASK)
	movwf	temp2
	; calculate start address
	savew	IINCHIP_MAP_TXBUF,stahi,stalo
	addwff	stahi,stalo,temp1,temp2
	; save threshold for buffer
	savew	IINCHIP_MAP_TXBUF+TXSIZE,counta,countb
sendbuf1:
	movpf	INDF0,WREG
	memw1f	stahi,stalo
	incwf	stahi,stalo
	; check for buffer overflow
	movfp	stahi,WREG
	cpfseq	counta
	goto	sendbuf2
	movfp	stalo,WREG
	cpfseq	countb
	goto	sendbuf2
	; buffer overflow
	savew	IINCHIP_MAP_TXBUF,stahi,stalo
sendbuf2:
	; check for end of our buffer
	decfsz	countc,f
	goto	sendbuf1
	; our buffer copied
	addwff	wrhi,wrlo,counth,countl
	movfp	wrhi,WREG
	memw1n	S0_TX_WR0
	movfp	wrlo,WREG
	memw1n	S0_TX_WR0+1
	memw1d	Sn_CR_SEND,S0_CR
	saveb	BUFINIT,FSR0
	return

; copy HTTP header to our buffer (interrupts must be disabled!)
sendhdr: 
	saveb	BUFINIT,FSR0
	savew	header,TBLPTRH,TBLPTRL
sendhdr1:
	memr	hibyte2 ; read word from program memory
	movwf	stemp ; save low byte
	; process high byte of the word
	movfp	hibyte2,WREG
	cpfslt	fzero
	return ; return from subroutine if 0
	movwf	INDF0
	; process low byte of the word
	movfp	stemp,WREG
	cpfslt	fzero
	return ; return from subroutine if 0
	movwf	INDF0
	goto	sendhdr1
	
; header must be <=112 characters and be multiple of 16
header:	data "HTTP/1.1 200 OK\nContent-type: text/plain      \n\n",0
;             0123456789ABCDE F0123456789ABCDEF0123456789ABCD E F

; send HTTP reply with DATA (interrupts must be disabled!)
getdata:
	savew	4,temp3,temp4
        bsf     CPUSTA,GLINTD ; disable interrupts
	movff	curhi,dstahi
	movff	curlo,dstalo
        bcf     CPUSTA,GLINTD ; enable interrupts
getdata1:
	clrwdt
	movlw	HIGH(RAMBADR)
	cpfseq	dstahi
	goto	getdata2
	movlw	LOW(RAMBADR)
	cpfseq	dstalo
	goto	getdata2
	savew	ROMBADR,dstahi,dstalo
getdata2:
	subwff	dstahi,dstalo,temp3,temp4
	movfp	dstahi,TBLPTRH
	movfp	dstalo,TBLPTRL
	memr	hibyte2
	movwf	stemp
	tstfsz	hibyte2
	goto	getdata3
	goto	getdata5
getdata3:	
	swapf	hibyte2,w
	call	conv_hex
	movwf	INDF0
	movfp	hibyte2,WREG
	call	conv_hex
	movwf	INDF0
	swapf	stemp,w
	call	conv_hex
	movwf	INDF0
	movfp	stemp,WREG
	call	conv_hex
	movwf	INDF0
	memr	hibyte2
	movwf	stemp
	swapf	hibyte2,w
	call	conv_hex
	movwf	INDF0
	movfp	hibyte2,WREG
	call	conv_hex
	movwf	INDF0
	swapf	stemp,w
	call	conv_hex
	movwf	INDF0
	movfp	stemp,WREG
	call	conv_hex
	movwf	INDF0
	memr	hibyte2
	movwf	stemp
	swapf	hibyte2,w
	call	conv_hex
	movwf	INDF0
	movfp	hibyte2,WREG
	call	conv_hex
	movwf	INDF0
	swapf	stemp,w
	call	conv_hex
	movwf	INDF0
	movfp	stemp,WREG
	call	conv_hex
	movwf	INDF0
	memr	hibyte2
	movwf	stemp
	swapf	hibyte2,w
	call	conv_hex
	movwf	INDF0
	movfp	hibyte2,WREG
	call	conv_hex
	movwf	INDF0
	movfp	stemp,WREG
	call	conv_hex
	movwf	INDF0
	movlw	'\n'
	movwf	INDF0
	tstfsz	FSR0
	goto	getdata4
	call	sendbuf
getdata4:	
	decwf	gethi,getlo
	movfp	gethi,WREG
	iorwf	getlo,w
	btfss	ALUSTA,Z
	goto	getdata1
getdata5:
	tstfsz	FSR0
	call	sendbuf
	return

; send HTTP reply with OK (interrupts must be disabled!)
getok:	saveb	'O',INDF0
	saveb	'K',INDF0
	saveb	'\n',INDF0
	call	sendbuf
	return

; send character WREG to COM-port
com_send:
	clrwdt
	movlb	1
	btfss	PIR^0x100,TXIF
	goto	com_send
	movlb	0
	movwf	TXREG
	return

; read 1 hexadecimal digit using INDF0 and save to WREG
readh:	movlw	0x30
	subwf	INDF0,w
	movpf	WREG,itmp^0x100
	movlw	0x09
	cpfsgt	itmp^0x100
	goto	readh1
	movlw	0x07
	subwf	itmp^0x100,f	
readh1:	movfp	itmp^0x100,WREG
	return

; read 4 bytes using INDF0 and save 2-byte word (iwhi, iwlo)
readw:	call	readh
	movwf	iwhi^0x100
	swapf	iwhi^0x100,f
	call	readh
	addwf	iwhi^0x100,f
	call	readh
	movwf	iwlo^0x100
	swapf	iwlo^0x100,f
	call	readh
	addwf	iwlo^0x100,f
	return
	
; send half byte (WREG) in hexadecimal form
sendh:	andlw	0x0F
	addlw	0x30
	movwf	itmp^0x100
	movlw	0x39
	cpfsgt	itmp^0x100
	goto	sendh_
	movfp	itmp^0x100,WREG
	addlw	0x07
	goto	sendh__
sendh_:
	movfp	itmp^0x100,WREG
sendh__:	
	call	com_send
	return

; send 2-byte word (iwhi, iwlo) in hexadecimal form
sendw:	swapf	iwhi^0x100,w
	call	sendh
	movfp	iwhi^0x100,WREG
	call	sendh
	swapf	iwlo^0x100,w
	call	sendh
	movfp	iwlo^0x100,WREG
	call	sendh
	return
	
; send new line
sendn:	callw	0x0D,com_send
	callw	0x0A,com_send
	return	
	
; send ERR and new line
sende:	callw	'E',com_send
	callw	'R',com_send
	callw	'R',com_send
	callw	0x0D,com_send
	callw	0x0A,com_send
	return	

; call far subroutine using iwhi and iwlo
fakecall:
	movfp	iwhi^0x100,PCLATH
	movfp	iwlo^0x100,PCL

; read 1 hexadecimal digit using INDF0 and save to WREG
readh0:	movlw	0x30
	subwf	INDF0,w
	movpf	WREG,rtemp
	movlw	0x09
	cpfsgt	rtemp
	goto	readh2
	movlw	0x07
	subwf	rtemp,f	
readh2:	movfp	rtemp,WREG
	return

; read 4 hex digits using INDF0 and save 2-byte word (gethi, getlo)
readw0:	call	readh0
	movwf	gethi
	swapf	gethi,f
	call	readh0
	addwf	gethi,f
	call	readh0
	movwf	getlo
	swapf	getlo,f
	call	readh0
	addwf	getlo,f
	return

; convert lower half byte of W to hexadecimal digit
conv_hex:
	andlw	0x0F
	addlw	0x30
	movwf	tmphex
	movlw	0x39
	cpfsgt	tmphex
	goto	conv_hex_
	movfp	tmphex,WREG
	addlw	0x07
	return
conv_hex_:
	movfp	tmphex,WREG
	return

; subprogram to delay 1 second
one_delay:
	movlw	100
	movwf	counta
del1s:	clrwdt
	; 10ms for 20MHz
	delay2	64,countb,countc 
	decfsz	counta,f
	goto	del1s
	return

; parse HTTP request and execute commands
qparse: saveb	BUFINIT,FSR0
	saveb	BUFSIZE,qtemp
	; looking for path
qloop:	movfp	INDF0,WREG
	sublw	'/'
	btfsc	ALUSTA,Z
	goto	qfound1
	decfsz	qtemp,f
	goto	qloop
	return
qfound1: ; path found
	movfp	INDF0,WREG
	movwf	qtemp
	movlw	'c'
	cpfseq	qtemp
	goto	qfound2
	; it's request to clear output
	movfp	INDF0,WREG
	movwf	qtemp
	movlw	'1'
	cpfseq	qtemp
	goto	qf1a
	; clear 1st output (RA2)
	bsf	PORTA,2
	clrf	stat1,f
	goto	qfound0
qf1a: 	movlw	'2'
	cpfseq	qtemp
	return ; do nothing if it's not 1/2
	; clear 2nd output (RA3)
	bsf	PORTA,3
	clrf	stat2,f
	goto	qfound0
qfound2:	
	movlw	's'
	cpfseq	qtemp
	goto	qfound3
	; it's request to set output
	movfp	INDF0,WREG
	movwf	qtemp
	movlw	'1'
	cpfseq	qtemp
	goto	qf2a
	; set 1st output (RA2)
	bcf	PORTA,2
	saveb	1,stat1
	goto	qfound0
qf2a: 	movlw	'2'
	cpfseq	qtemp
	return ; do nothing if it's not 1/2
	; set 2nd output (RA3)
	bcf	PORTA,3
	saveb	1,stat2
	goto	qfound0
qfound3:
	movlw	' '
	cpfseq	qtemp
	goto	qfound4
	; it's request for all data
	clrf	getlo,f
	movlw	0x08
	movwf	gethi
	call	sendhdr ; HTTP header
	call	getdata ; get 0x0800 records
	return ; return from qparse subroutine
qfound4: ; probably it's request for specified number of records
	decf	FSR0,f ; step back
	call	readw0 ; save number to gethi/getlo
	call	sendhdr ; HTTP header
	call	getdata ; get specified number of records
	return ; return from qparse subroutine
qfound0: ; send back "OK" mesage
	call	sendhdr ; HTTP header
	call	getok 
	return ; return from qparse subroutine

main:
        clrf    ALUSTA,f	; clear ALUSTA
        bsf     ALUSTA,FS3      ; no auto increment FSR1
        bcf     ALUSTA,FS1      ; auto increment FSR0
	bsf	ALUSTA,FS0
        bsf     CPUSTA,GLINTD   ; disable interrupts
	movlb   0		; bank 0
        clrf	PORTA,f		; clear port A
	bsf	PORTA,2		; set "1" to RA2
	bsf	PORTA,3		; set "1" to RA3
	bcf     PORTA,NOT_RBPU	; enable weak pull ups for port B
	saveb	0xFF,DDRB	; port B all inputs
	saveb	0x08,INTSTA	; enable peripheral interrupts
	movlr	1		; register bank 1
	saveb	BUFINIT,ibuf^0x100 ; setup buffer for RS-232
	movlr	0		; register bank 0
	movlw	32 		; RS-232 : 9600 on 20 MHz
	movwf	SPBRG
	movlw	0x20
	movwf	TXSTA
	movlw	0x90
	movwf	RCSTA
	movlb	1
	movlw	0x01
	movwf	PIE^0x100
	movlb	0
	memw1d	0x02,0x300B ; RTC: disable SQWE, enable 24
	memw1d	0x30,0x300A ; RTC: enable extended regs, enable oscillator
	memw1d	0x00,0x304B ; RTC: disable E32K
	memw1d	0x20,0x300A ; RTC: disable extended regs, enable oscillator
	memr1n	0x3010 ; RTC: read NVM value for SQWE frequency
	tstfsz	WREG ; test for 0 that means network and external interrupts disabled
	goto	hinit1
	bcf     CPUSTA,GLINTD ; enable interrupts
	goto	mainloop
hinit1:	andlw	0x0F ; use only lower half of byte
	iorlw	0x20 ; add 0x20 to enable oscillator
	memw1n	0x300A ; RTC: set SQWE frequency
	memw1d	0x0A,0x300B ; RTC: enable SQWE, enable 24
	saveb	0x80,T0STA ; external interrupt by raising edge (inverted /INT from WizNET)
	saveb	0x0D,INTSTA ; allow peripheral and 2 external interrupts (RA0 and RA1)
	movlr	0 ; 1st bank for file of registers
	clrf	iflag,f ; clear signal register
	clrf	fzero,f ; clear zero register
	clrf	isqwe,f ; clear sqwe counter
	bcf     CPUSTA,GLINTD ; enable interrupts
	savew	RAMBADR,curhi,curlo 
	savew	RAMBADR,TBLPTRH,TBLPTRL
	savew	PAGSIZE,counth,countl
clram:	clrwdt
	clrf	hibyte2,w
	memw	hibyte2 ; clear external word
	decwf	counth,countl
	movfp	counth,WREG
	iorwf	countl,w
	btfss	ALUSTA,Z
	goto	clram
	
; initialize WizNET

	memw1d	0x00,MR
	memw1d	IR_SOCK0,IMR ; enable socket 0 interrupt
	memw1d	0x03,RMSR ; socket 0 - 8K
	memw1d	0x03,TMSR ; socket 0 - 8K

; copy values from NVM to WizNET registers

	saveb	18,ncount ; 18 bytes: 4-Gateway, 4-SubnetMask, 6-MAC, 4-IP
	savew	NVMINET,asrchi,asrclo
	savew	WIZINET,adsthi,adstlo
ncopy:	memr1f	asrchi,asrclo
	memw1f	adsthi,adstlo
	incwf	asrchi,asrclo
	incwf	adsthi,adstlo
	decfsz	ncount,f
	goto	ncopy

create:	clrwdt ; clear watchdog timer
	clrf	nerr,f ; clear error number

; create socket

	memw1d	Sn_MR_TCP|Sn_MR_ND,S0_MR
	memr1n	NVMPHI ; high byte of default port from NVM
	memw1n	S0_PORT0
	memr1n	NVMPLO ; low byte of default port from NVM
	memw1n	S0_PORT0+1
	memw1d	Sn_CR_OPEN,S0_CR
	memtst	S0_SR,SOCK_INIT,error1

; start listening

	memw1d	Sn_CR_LISTEN,S0_CR
	memtst	S0_SR,SOCK_LISTEN,error2	

mainloop:
	delay10ms
	; check for request
	tstfsz	iflag
	goto	request
	call	checkcom	
	goto	mainloop
request: ; we have request
	call	qparse
	clrf	iflag,f ; clear signal register
	memw1d	Sn_CR_DISCON,S0_CR
	delay100ms
	memw1d	Sn_CR_CLOSE,S0_CR
	saveb	SOCK_CLOSED,temp
closed: clrwdt
	; check socket interrupts are presented
	memr1n	S0_IR
	tstfsz	WREG
	call	clearir
	; check socket is closed
	memr1n	S0_SR
	cpfseq	temp
	goto	closed
	; socket 0 closed and cleared
	memw1d	IR_SOCK0,IMR ; enable socket 0 interrupt again
	goto	create ; go to create socket 0
clearir: ; clear S0_IR
	memw1n	S0_IR
	return

error5:	saveb	'5',nerr
	goto	error0
error4:	saveb	'4',nerr
	goto	error0
error3:	saveb	'3',nerr
	goto	error0
error2:	saveb	'2',nerr
	goto	error0
error1:	saveb	'1',nerr
error0:	memw1d	Sn_CR_CLOSE,S0_CR
	callw	'E',com_send
	callw	'R',com_send
	callw	'R',com_send
	movfp	nerr,WREG
	call	com_send
	callw	'\r',com_send
	callw	'\n',com_send
	memw1d	IR_SOCK0,IMR ; enable socket 0 interrupt
	goto	create

checkcom:
	clrf	WREG,f
	movlr	1
	cpfseq	ibuf^0x100
	goto	loop1
	; command
	movlw	BUFINIT
	movwf	FSR0
	movpf	INDF0,itmp^0x100
	movlw	'?'
	cpfseq	itmp^0x100
	goto	loop01
	; command READ
	call	readw
	movfp	iwhi^0x100,WREG
	movwf	TBLPTRH
	movfp	iwlo^0x100,WREG
	movwf	TBLPTRL
	movpf	INDF0,itmp^0x100
	movlw	'='
	cpfseq	itmp^0x100
	goto	loop0r
	call	readw
	movfp	iwhi^0x100,WREG
	movwf	ichi^0x100
	movfp	iwlo^0x100,WREG
	movwf	iclo^0x100
	goto	loop0rr	
loop0r:	clrf	ichi^0x100,f
	movlw	0x01
	movwf	iclo^0x100
loop0rr:
	memr	iwhi^0x100
	movwf	iwlo^0x100
	call	sendw
	callw	' ',com_send
	tstfsz  iclo^0x100
	goto	loop0rrr
	decf    ichi^0x100,f
loop0rrr:	
	decf    iclo^0x100,f
	movfp	iclo^0x100,WREG
	iorwf	ichi^0x100,w
	btfss	ALUSTA,Z
	goto	loop0rr
	call	sendn
	goto	loop00
loop01: movlw	'!'
	cpfseq	itmp^0x100
	goto	loop02
	; command WRITE
	call	readw
	movfp	iwhi^0x100,WREG
	movwf	TBLPTRH
	movfp	iwlo^0x100,WREG
	movwf	TBLPTRL
	movpf	INDF0,itmp^0x100
	movlw	'='
	cpfseq	itmp^0x100
	goto	loop0w
loop0www:
	call	readw
	movfp	iwhi^0x100,WREG
	movwf	ichi^0x100
	movfp	iwlo^0x100,WREG
	movwf	iclo^0x100
	goto	loop0ww
loop0w:	decf	FSR0,f
	clrf	INDF0,f
	decf	FSR0,f
	clrf	ichi^0x100,f
	clrf	iclo^0x100,w
loop0ww:
	memw	ichi^0x100
	movpf	INDF0,WREG
	btfsc	ALUSTA,Z
	goto	loop00
	decf	FSR0,f
	goto	loop0www
loop02: movlw	'$'
	cpfseq	itmp^0x100
	goto	loop00
	; command GOTO
	call	readw
	call	fakecall
	goto	loop00
loop0e:	call	sende
loop00:	movlr	0
	callw	'>',com_send	
	movlr	1
	movlw	BUFINIT
	movwf	ibuf^0x100
	return
loop1:	movlw	BUFINIT
	movpf	WREG,FSR0
loop2:	movfp	ibuf^0x100,WREG
	cpfslt	FSR0
	return
	movpf	INDF0,WREG
	movlr	0
	movlr	1
	goto	loop2
    
	END
            